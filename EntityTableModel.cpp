#include "EntityTableModel.h"
#include <QDebug>

EntityTableModel::EntityTableModel(Semestrator& semestrator, QObject *parent) :
    QAbstractTableModel(parent),
    semestrator(semestrator)
{
    QObject::connect(&semestrator, SIGNAL(dataChanged()), this,SLOT(invokeRefresh()));
}

void EntityTableModel::invokeRefresh(){
    qDebug()<<"invoking refresh";

    beginResetModel();
    endResetModel();
}


