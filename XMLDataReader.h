/* 
 * File:   XMLDataReader.h
 * Author: nickl
 *
 * Created on March 23, 2014, 3:44 PM
 */

#ifndef XMLDATAREADER_H
#define	XMLDATAREADER_H

#include <Qt>
#include <QDebug>
#include <QtCore/qlogging.h>
#include <QApplication>
#include <QXmlStreamReader>
#include <QFile>
#include <QDate>
#include "Items.h"
#include "LogicException.h"

#define levelRead(tagname, body)  while (xml.readNextStartElement()) \
{\
    if (xml.name() == tagname) \
        body \
    else \
        xml.skipCurrentElement();\
}  

class XMLDataReader
{
    QHash<int, Group> groups;
    QHash<int, Chair> chairs;
    QHash<int, Subject> subjects;
    QHash<int, StudyType> studyTypes;
    QHash<int, Room> rooms;
    QHash<int, Teacher> teachers;
    QHash<int, QList<Shed >> sheds;
    QHash<int, Load> loads;

    QHash<QString, QString> readOneLevelMap(QSet<QString> attrs);

    QList<int> readOneLevelIntList(QString name);

    void readClasses();

    void readSubjects();

    void readRooms();

    /*
    <chair> <!-- кафедра -->
        <id>0</id> <!-- id -->
        <external_id>-1</external_id>
        <short_name>АСОИиУ</short_name> <!-- короткое название, max len 15 -->
        <full_name>Автоматиз. системы обработки инф. и управления</full_name> <!-- полное название, max len 50 -->
    </chair>
     */

    void readChairs();

    void readOneTeacher();

    void readScheds();

    void readStudyType();

    void readLoad();

    LoadPart readLoadPart();


public:
    QXmlStreamReader xml;


    void clearData();


    void loadXml(QString name);

    const QHash<int, Group>& getGroups() const
    {
        return groups;
    }

    const QHash<int, Subject>& getSubjects() const
    {
        return subjects;
    }

    const QHash<int, Room>& getRooms() const
    {
        return rooms;
    }

    const QHash<int, Teacher>& getTeachers() const
    {
        return teachers;
    }

    const QHash<int, Chair>& getChairs() const
    {
        return chairs;
    }

    const QHash<int, QList<Shed> >& getSheds() const
    {
        return sheds;
    }

    const QHash<int, StudyType>& getStudyTypes() const
    {
        return studyTypes;
    }

    const QHash<int, Load>& getLoads() const
    {
        return loads;
    }

    QList<LoadPart> getLoadForGroup(int groupId);

    QList<LoadPart> getLoadForTeacher(int teacher_id);

    QList<LoadPart> getLoadForRoom(int room_id);



};

#endif	/* XMLDATAREADER_H */

