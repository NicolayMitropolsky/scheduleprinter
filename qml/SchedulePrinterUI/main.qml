import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.0
import QtQuick.Window 2.1
import QtQuick.Dialogs 1.1

ApplicationWindow {
    property var _ROOMS_WEEKLY: 3;
    property var _MULTIPLE_ROOMS_EXTENDED: 4
    property var _EXAMS: 5
    visible: true
    title: "Печать расписанsий"
    property int margin: 11
    width: settings.value("window.width", mainLayout.implicitWidth + 2 * margin)
    height: settings.value("window.height",mainLayout.implicitHeight + 2 * margin)
    minimumWidth: 500
    minimumHeight: mainLayout.Layout.minimumHeight + 2 * margin
    y: settings.value("window.y",(Screen.desktopAvailableHeight - height) / 2)
    x: settings.value("window.x",(Screen.desktopAvailableWidth - width) / 2)


    onClosing: {
        settings.setValue("lastxmlfile",fileNameLabel.text)
        settings.setValue("window.x",x)
        settings.setValue("window.y",y)
        settings.setValue("window.width",width)
        settings.setValue("window.height",height)
    }

    Component.onCompleted: {
        semestrator.error.connect(showError)
    }

    function showError(message) {
        console.log("Alert: " + message)
        messageDialog.text = message
        messageDialog.open()
    }
    function animateCompletation() {
        createAnimation.restart();
    }
    function processExport(){
        var selected_ids=[];
        itemsTable.selection.forEach(
                    function(rowIndex) {
                        console.log(rowIndex)
                        var mappedIndex = itemsTable.model.mapToSourceRow(rowIndex)
                        console.log("mappedIndex="+mappedIndex)
                        var g = itemsTable.emodel.getId(mappedIndex)
                        selected_ids.push(g)
                    }
                    )

        if(combo.currentIndex == _ROOMS_WEEKLY){
            console.log("itemsTable.selection=",itemsTable.selection)
            exporterManager.writeRoomsWeekly(selected_ids);
        } else if(combo.currentIndex == _MULTIPLE_ROOMS_EXTENDED){
            console.log("itemsTable.selection=",itemsTable.selection)
            exporterManager.writeMultipleRoomsExtended(selected_ids);
        }
        else if(combo.currentIndex >= _EXAMS)
        {
            exporterManager.writeExam(itemsTable.emodel.ename,selected_ids);
        }
        else
        {
            exporterManager.write(itemsTable.emodel.ename,selected_ids);
        }
    }

    MessageDialog {
        id: messageDialog
        title: "Ошибка"
        text: "Сообщение"
        icon :StandardIcon.Warning
        onAccepted: {

        }
    }

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent
        anchors.margins: margin
        FileDialog {
            id: fileDialog
            //nameFilters: [ "Image files (*.png *.jpg)" ]
            onAccepted: {
                fileNameLabel.text = semestrator.urlToFilePath(fileUrl)
            }
        }
        RowLayout {
            id: rowLayout
            anchors.fill: parent
            Label{text:"XML-файл:"}
            TextField {
                id: fileNameLabel
                placeholderText: "Имя файла"
                //text:"/home/nickl/Yandex.Disk/Расписание МГТУ СТАНКИН/Файлы/Расписание Весна 2014.xml"
                text: settings.value("lastxmlfile","")
                Layout.fillWidth: true

            }
            Button {
                focus: true
                text: "Выбрать"
                onClicked: {
                    fileDialog.open()
                }
            }
            Button {
                text: "Загрузить"
                onClicked: {
                    semestrator.loadXml(fileNameLabel.text)
                }
            }
        }
        RowLayout {
            Label{text:"Сущность:"}

            ComboBox {
                id: combo
                focus: true
                model:   ListModel {
                    id: choices
                    ListElement { text: "Группы" }
                    ListElement { text: "Преподаватели" }
                    ListElement { text: "Аудитории" }
                    ListElement { text: "Аудитории по неделям" }
                    ListElement { text: "Аудитории по неделям(расширенная)" }
                    ListElement { text: "Группы (экз)" }
                    ListElement { text: "Преподаватели (экз)" }
                }
                currentIndex: 0
                Layout.fillWidth: true
                onActivated: {
                    console.log(index);
                    //itemsTable.model = classesTableModel
                    switch(index){
                    case 0: itemsTable.setEntityModel(gropsTM); break
                    case 1: itemsTable.setEntityModel(theacherTM); break
                    case 2: itemsTable.setEntityModel(roomTM); break
                    case 3: itemsTable.setEntityModel(roomTM); break
                    case 4: itemsTable.setEntityModel(roomTM); break
                    case 5: itemsTable.setEntityModel(gropsTM); break
                    case 6: itemsTable.setEntityModel(theacherTM); break
                    }
                }
            }
        }
        RowLayout {
            Label{text:"Поиск:"}
            TextField {
                placeholderText: "Название"
                Layout.fillWidth: true
                inputMethodHints: Qt.ImhNoPredictiveText;
                onTextChanged: {
                    console.log("filterText:", text)
                    proxyModel.setFilterFixedString(text)
                }
            }
        }
        TableView {
            id: itemsTable
            Layout.fillHeight: true
            Layout.fillWidth: true
            headerVisible: true
            sortIndicatorVisible : true;
            selectionMode: SelectionMode.ExtendedSelection
            onSortIndicatorColumnChanged: model.sort(sortIndicatorColumn, sortIndicatorOrder)
            onSortIndicatorOrderChanged: model.sort(sortIndicatorColumn, sortIndicatorOrder)

            TableViewColumn {title: "100"; role: "display";}
            //            TableViewColumn {title: "2"; role: "display"; width: 70   }
            //            TableViewColumn {title: "3"; role: "display"; width: 70 }

            //            TableViewColumn{ role: "modelData"  ; title: "Title" ; width: 100 }
            //            TableViewColumn{ role: "author" ; title: "Author" ; width: 200 }
            model: proxyModel
            property var emodel: gropsTM
            function rebuildColumns(){
                var cc = columnCount;
                console.log("columnCount:",cc);
                for(var j = 0; j < cc; j++){
                    console.log("removing:",j);
                    removeColumn(0);
                }
                console.log("itemsTable.model.columnCount="+itemsTable.model.columnCount);
                for(var i = 0; i < itemsTable.model.columnCount; i++){
                    console.log("loading column", itemsTable.model.headerName(i));
                    addColumn( Qt.createQmlObject('import QtQuick 2.2;import QtQuick.Controls 1.1;
TableViewColumn {title: "'+itemsTable.model.headerName(i)+'"; role: "column'+i+'";}',itemsTable,"ii"
                                                  ));
                }

            }

            function setEntityModel(emodel) {
                proxyModel.setSourceModel(emodel)
                itemsTable.emodel = emodel
                rebuildColumns();
                //proxyModel.sort(0);
            }
            Component.onCompleted: {
                setEntityModel(gropsTM)
            }
            onDoubleClicked: {
                exporterManager.setPrintingOutput();
                processExport();
            }
        }
        RowLayout {
            Label{text:"Выходной формат:"}
            ComboBox {
                id: exportombo
                focus: true
                model:   ListModel {
                    ListElement { text: "PDF" }
                    //   ListElement { text: "HTML" }
                    //   ListElement { text: "Печать" }
                }
                currentIndex: 0
                Layout.fillWidth: true
            }
        }
        RowLayout {
            Button {
                text: "Печать"
                onClicked: {
                    console.log("choices.currentIndex",combo.currentIndex)
                    exporterManager.setPrintingOutput();
                    processExport();
                    animateCompletation();
                }
            }

            Button {
                text: "Экспортировать"
                onClicked: {
                    console.log("choices.currentIndex",combo.currentIndex)
                    exporterManager.setPDFOutput();
                    processExport();
                    animateCompletation();
                }
            }
            Label{
                text:"Выполнено"
                color: "green"
                opacity: 0
                Layout.fillWidth: true
                NumberAnimation on opacity {
                    running: false
                    id: createAnimation
                    from: 1
                    to: 0
                    duration: 1000
                }

            }
            Button {
                text: "Открыть папку"
                anchors.right: parent.right
                anchors.left: undefined
                onClicked: {
                    exporterManager.openLastExportFolder();
                }
            }
        }



    }

    statusBar: StatusBar{
        RowLayout{
            Text {
                anchors.right: parent.right
                anchors.left: undefined
                text: 'Исходные коды: <a href="http://bitbucket.org/NicolayMitropolsky/scheduleprinter/">bitbucket.org/NicolayMitropolsky/scheduleprinter/</a>'
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
    }
}
