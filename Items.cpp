
#include "Items.h"

QDebug &operator<<(QDebug &stream, const Group& t)
{
    stream << "Group(" << t.id << ", " << t.name << ")";
    return stream;
}

QDebug &operator<<(QDebug &stream, const Subject& t)
{
    stream << "Subject(" << t.id << ", " << t.short_name << ", " << t.full_name << ")";
    return stream;
}

QDebug &operator<<(QDebug &stream, const Room& t)
{
    stream << "Room(" << t.id << ", " << t.name << ")";
    return stream;
}

QDebug &operator<<(QDebug &stream, const Teacher& t)
{
    stream << "Teacher(" << t.id << ", " << t.surname << ", " << t.first_name << ", " << t.second_name << ")";
    return stream;
}

QDebug &operator<<(QDebug &stream, const Chair& t)
{
    stream << "Chair(" << t.id << ", " << t.short_name << ", " << t.full_name << ")";
    return stream;
}


QDebug &operator<<(QDebug &stream, const Shed& t)
{
    stream << "Shed(" << t._day << ", " << t._hour << ", " << t._loadPart << ", " << t._room_id <<
              ", " << t._load_id << ", " << t._begin_date << ", " << t._end_date << ")";
    return stream;
}

QDebug &operator<<(QDebug &stream, const LongShed& t)
{
    stream << "LongShed(" << t.day() << ", " << t.hours() << ", " << t.loadPart() << ", " << t.room_id() <<
              ", " << t.load_id() << ", " << t.begin_date() << ", " << t.end_date() << ")";
    return stream;
}

QDebug &operator<<(QDebug &stream, const StudyType& t)
{
    stream << "StudyType(" << t.id << ", " << t.short_name << ", " << t.full_name << ")";
    return stream;
}

QDebug &operator<<(QDebug &stream, const LoadPart& t)
{
    stream << "LoadPart(loadid=" << t.loadid << ", room_id_list=" << t.room_id_list << ", hour_per_week_list=" << t.hour_per_week_list << ")";
    return stream;
}

QDebug &operator<<(QDebug &stream, const Load& t)
{
    stream << "Load(" << t.id << ", groupsIds=" << t.groupsIds << ", parts=" << t.parts << ")";
    return stream;
}


QDebug &operator<<(QDebug &stream, const SemesterTimeTableCell& t)
{
    stream << "SemesterTimeTableCell(day=" << t.day << ", hours=" << t.hours
           << ", place=" << t.place
           << ", teacher=" << t.teacher
           << ", subject=" << t.subject
           << ", studyType=" << t.studyType
           << ", groups=" << t.groups
           << ", dates=" << t.dates
           << ")";
    return stream;
}
