#include "MultipleRoomsExporter.h"


void MultipleRoomsExporter::setData(const QList< SemesterTimeTableCell >& data)
{
    reinit();

    //qDebug() << "setData arg=" << data;

    timetableByDay.clear();

    for (const SemesterTimeTableCell& cell : data)
    {
        for(int hour : cell.hours){
            timetableByDay[cell.place.id][cell.day][hour].append(cell);
        }
    }
}
void MultipleRoomsExporter::runExport(QPrinter& printer, QPainter& painter)
{

    //qDebug() << "data=" << timetableByDay;

    verts = QList<int>();
    hors = QList<int>();
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);


    QPen pen;
    pen.setWidth(15);

    painter.setPen(pen);

    qDebug()<<"maxWidth = "<<maxWidth;
    qDebug()<<"daysInPage = "<<daysInPage();
    qDebug()<<"roomsInPage = "<<roomsInPage();


    QList<QList<Room>> grRooms = groupRooms(rooms, roomsInPage());

    //        if(fromPage == 0)
    //            fromPage = 1;
    //        if(toPage == 0 || toPage > grRooms.size())
    //            toPage = grRooms.size();

    qDebug()<<"fromPage = "<<fromPage<<" toPage = "<<toPage;

    int page = 1;

    for(int g = 0; g < grRooms.size(); ++g){

        qDebug()<<"page = "<<page;

        if(toPage > 0 && page > toPage)
            break;

        QList<Room> gr = grRooms[g];

        qDebug()<<"gr = "<<gr;

        if(page >= fromPage)
            writeRooms(painter, gr);

        int printedDays = 0;

        for(QDate curdate = from; curdate <= to; curdate = curdate.addDays(1))
        {
            if(curdate.dayOfWeek() == 7)
                continue;

            if(printedDays == daysInPage()){
                if(page >= fromPage){
                    drawBoundary(painter, printedDays, gr.size());
                    printer.newPage();
                }
                page++;
                printedDays = 0;
                if(toPage > 0 && page > toPage)
                    break;
                if(page >= fromPage)
                    writeRooms(painter, gr);
            }
            if(page >= fromPage)
                writeDay(painter, gr, curdate, hx + roomNameswidth + dayWidth()*printedDays, hy);
            printedDays++;
        }

        if(toPage > 0 && page > toPage)
            break;
        if(page >= fromPage)
            drawBoundary(painter, printedDays, gr.size());

        if(g < grRooms.size()-1)
        {
            if(page >= fromPage)
                printer.newPage();
            page++;
        }
    }

    toPage = page;

}
QList< QList< Room > > MultipleRoomsExporter::groupRooms(QList< Room > rooms, int size)
{
    QList<QList<Room>> result;

    QList<Room> item;

    for(int i = 0; i < rooms.size();)
    {
        item.append(rooms[i]);
        i++;
        if(i % size == 0 && i != rooms.size())
        {
            result.append(item);
            item = QList<Room>();
        }

    }

    result.append(item);
    return result;
}
void MultipleRoomsExporter::writeRooms(QPainter& painter, QList< Room > rooms) {

    int bx = hx;
    int by = hy + headerHeight;

    for(int i = 0; i < rooms.size(); ++i){

        int y1 = by + cellheight() * i;
        int x1 = bx;
        int y2 = y1;//x1 + 30 * v;
        int x2 = x1 + roomNameswidth;

        painter.drawLine(x1,y1,x2,y2);
        painter.drawText(x1+margin, y1+margin,
                         roomNameswidth-margin, cellheight()-margin,
                         Qt::AlignVCenter | Qt::AlignLeft,
                         rooms[i].name+" ("+QString::number(rooms[i].capacity)+")");

    }

    painter.drawLine(hx, hy, hx, by + rooms.size() * cellheight());
    painter.drawLine(hx+ roomNameswidth, hy, hx + roomNameswidth, by + rooms.size() * cellheight());

}
void MultipleRoomsExporter::drawBoundary(QPainter& painter, int printedDays, int roomsSize) {
    painter.drawRect(hx,
                     hy,
                     roomNameswidth + printedDays * dayWidth(),
                     headerHeight + roomsSize * cellheight()
                     );
}
void MultipleRoomsExporter::drawTimeHeader(int dx, int dy, QPainter& painter, int j) {
    drawRotatedText(painter,
                    dx + pointCellWidth() * j,
                    dy + headerHeight,
                    (headerHeight - subheader),
                    pointCellWidth(),
                    hours[j]
                    );


}
void MultipleRoomsExporter::writeDay(QPainter& painter, QList< Room > rooms, QDate curdate, int dx, int dy) {

    qDebug()<<"curdate="<<curdate;

    painter.drawText(dx+margin, dy+margin,
                     dayWidth() - margin, subheader-margin,
                     //300,300,
                     Qt::AlignVCenter | Qt::AlignCenter,
                     curdate.toString(headingDateFormat()));

    painter.drawLine(dx, dy, dx ,dy + headerHeight);

    painter.drawLine(dx, dy +subheader, dx + dayWidth(),dy +subheader);

    QFont f = painter.font();

    painter.setFont(getFont(8));
    for(int j = 0; j < hoursIndxs.size(); ++j){
        painter.drawLine(
                    dx+ pointCellWidth() * j,
                    dy +subheader,
                    dx+ pointCellWidth() * j,
                    dy +headerHeight
                    );

        drawTimeHeader(dx, dy, painter, j);


    }

    painter.setFont(f);


    for(int i = 0; i < rooms.size(); ++i){
        int y1 = dy + headerHeight + cellheight() * i;
        int y2 = y1;

        painter.drawLine(dx, y1, dx + dayWidth(), y1);

        Room room = rooms[i];

        for(int j = 0; j < hoursIndxs.size(); ++j){
            int x1 = dx + pointCellWidth() * j;
            int x2 = x1 + pointCellWidth();

            painter.drawLine(x1, y1, x1 , y1 + cellheight());

            QList<SemesterTimeTableCell > celldata = timetableByDay[room.id][curdate.dayOfWeek()][hoursIndxs[j]];


            //                painter.drawText(x1+margin, y1+margin,
            //                        roomNameswidth-margin, cellheight-margin,
            //                        Qt::AlignVCenter | Qt::AlignLeft,
            //                        QString::number(celldata.size()));

            if(containsDate(celldata, curdate))
                paintCell(painter, x1, y1, curdate, celldata);

        }

    }

}
void MultipleRoomsExporter::paintCell(QPainter& painter, int x1, int y1, const QDate& curdate, const QList< SemesterTimeTableCell >& celldata)
{
    painter.fillRect(x1 + margin,
                     y1 + margin,
                     pointCellWidth() - margin * 2,
                     cellheight() - margin * 2,
                     Qt::black);
}
bool MultipleRoomsExporter::containsDate(const QList< SemesterTimeTableCell >& celldata, const QDate& date) {

    for(const SemesterTimeTableCell& cell: celldata){
        const QList<QDate>& dates = cell.dates;
        if(std::binary_search(dates.begin(), dates.end(),date))
            return true;
    }

    return false;

}
