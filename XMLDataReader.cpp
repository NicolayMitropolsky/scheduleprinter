/* 
 * File:   XMLDataReader.cpp
 * Author: nickl
 *
 * Created on March 23, 2014, 3:44 PM
 */

#include "XMLDataReader.h"



QHash< QString, QString > XMLDataReader::readOneLevelMap(QSet< QString > attrs)
{

    QHash<QString, QString> result;

    while (xml.readNextStartElement())
    {
        QString arrtsname = xml.name().toString();
        if (attrs.contains(arrtsname))
            result.insert(arrtsname, xml.readElementText());
        else
            xml.skipCurrentElement();
    }

    return result;
}
QList< int > XMLDataReader::readOneLevelIntList(QString name)
{

    QList<int> result;

    while (xml.readNextStartElement())
    {
        if (xml.name() == name)
            result.append(xml.readElementText().toInt());
        else
            xml.skipCurrentElement();
    }

    return result;
}
void XMLDataReader::readClasses()
{
    while (xml.readNextStartElement())
    {
        if (xml.name() == "class")
        {
            QHash<QString, QString> olm = readOneLevelMap({"id", "name", "semester"});
            //qDebug() << "olm=" << olm;
            Group gr(olm["id"].toInt(), olm["name"], olm["semester"].toInt());
            groups.insert(gr.id, gr);

        }
        else
            xml.skipCurrentElement();
    }

    qDebug() << "After readClassestag=" << xml.name();
}
void XMLDataReader::readOneTeacher()
{

    QHash<QString, QString> olm;
    int chair_id = -2;

    while (xml.readNextStartElement())
    {
        if (xml.name() == "person")
        {
            olm = readOneLevelMap({"id", "surname", "first_name", "second_name"});
            //qDebug() << "olm=" << olm;

        } else if(xml.name() == "chair_id")
        {
            chair_id = xml.readElementText().toInt();
        }
        else
            xml.skipCurrentElement();
    }

    Teacher gr(olm["id"].toInt(), olm["surname"], olm["first_name"], olm["second_name"], chair_id);
    teachers.insert(gr.id, gr);

}
void XMLDataReader::readChairs()
{
    while (xml.readNextStartElement())
    {
        if (xml.name() == "chair")
        {
            QHash<QString, QString> olm = readOneLevelMap({"id", "short_name", "full_name"});
            //qDebug() << "olm=" << olm;
            Chair gr(olm["id"].toInt(), olm["short_name"], olm["full_name"]);
            chairs.insert(gr.id, gr);

        }
        else
            xml.skipCurrentElement();
    }

}
void XMLDataReader::readRooms()
{
    while (xml.readNextStartElement())
    {
        if (xml.name() == "room")
        {
            QHash<QString, QString> olm = readOneLevelMap({"id", "name", "capacity"});
            //qDebug() << "olm=" << olm;
            Room gr(olm["id"].toInt(), olm["name"], olm["capacity"].toInt());
            rooms.insert(gr.id, gr);

        }
        else
            xml.skipCurrentElement();
    }

}
void XMLDataReader::readSubjects()
{
    while (xml.readNextStartElement())
    {
        if (xml.name() == "subject")
        {
            QHash<QString, QString> olm = readOneLevelMap({"id", "short_name", "full_name"});
            //qDebug() << "olm=" << olm;
            Subject gr(olm["id"].toInt(), olm["short_name"], olm["full_name"]);
            subjects.insert(gr.id, gr);

        }
        else
            xml.skipCurrentElement();
    }

}
void XMLDataReader::readScheds()
{
    while (xml.readNextStartElement())
    {
        if (xml.name() == "sched")
        {

            QHash<QString, QString> olm = readOneLevelMap({
                                                              "day", "hour", "group", "room_id",
                                                              "load_id", "begin_date", "end_date"
                                                          }
                                                          );
            //qDebug() << "olm=" << olm;
            //QDate begin_date =;

            Shed gr(
                        olm["day"].toInt(),
                    olm["hour"].toInt(),
                    olm["group"].toInt(),
                    olm["room_id"].toInt(),
                    olm["load_id"].toInt(),
                    QDate::fromString(olm["begin_date"], "dd.MM.yyyy"),
                    QDate::fromString(olm["end_date"], "dd.MM.yyyy")
                    );
            sheds[gr.load_id()].append(gr);

        }
        else
            xml.skipCurrentElement();
    }

}
void XMLDataReader::readStudyType()
{
    QHash<QString, QString> olm = readOneLevelMap({"id", "short_name", "full_name"});
    //qDebug() << "olm=" << olm;
    StudyType gr(olm["id"].toInt(), olm["short_name"], olm["full_name"]);
    studyTypes.insert(gr.id, gr);

}
void XMLDataReader::readLoad()
{

    int id = -1;
    QList<int> klass_id_list;
    QList<LoadPart> loadparts;

    while (xml.readNextStartElement())
    {
        if (xml.name() == "id")
        {
            id = xml.readElementText().toInt();
        }
        else if (xml.name() == "klass_id_list")
        {
            klass_id_list = readOneLevelIntList("int");
        }
        else if (xml.name() == "groups")
        {
            while (xml.readNextStartElement())
            {
                if (xml.name() == "group")
                    loadparts.append(readLoadPart());
                else
                    xml.skipCurrentElement();
            }
        }
        else
            xml.skipCurrentElement();
    }

    Q_ASSERT(id != -1);

    for (int i = 0; i < loadparts.size(); i++)
    {
        loadparts[i].loadid = id;
        loadparts[i].loadPartsCount = loadparts.size();
        loadparts[i].loadPartIndex = i;
    }

    loads.insert(id, Load(id, loadparts, klass_id_list));

}
LoadPart XMLDataReader::readLoadPart()
{


    int teacher_id = -1;
    int subject_id = -1;
    int study_type_id = -1;

    QList<int> room_id_list;
    QList<int> hour_per_week_list;

    while (xml.readNextStartElement())
    {
        if (xml.name() == "teacher_id")
        {
            teacher_id = xml.readElementText().toInt();
        }
        else if (xml.name() == "subject_id")
        {
            subject_id = xml.readElementText().toInt();
        }
        else if (xml.name() == "study_type_id")
        {
            study_type_id = xml.readElementText().toInt();
        }
        else if (xml.name() == "room_id_list")
        {
            room_id_list = readOneLevelIntList("int");
        }
        else if (xml.name() == "hour_per_week_list")
        {
            hour_per_week_list = readOneLevelIntList("int");
        }
        else
            xml.skipCurrentElement();
    }

    return LoadPart(teacher_id, subject_id, study_type_id, room_id_list, hour_per_week_list);

}
void XMLDataReader::clearData() {
    this->xml.clear();
    this->groups.clear();
    this->loads.clear();
    this->rooms.clear();
    this->sheds.clear();
    this->studyTypes.clear();
    this->subjects.clear();
    this->teachers.clear();
}
void XMLDataReader::loadXml(QString name)
{

    QFile f(name);
    if (!f.open(QFile::ReadOnly))
    {
        qWarning() << "Cant find file:" << name;
        throw LogicException("Файл не найден");
    }

    xml.setDevice(&f);

    if (!xml.atEnd() && xml.readNextStartElement())
    {
        qDebug() << "loadXmlCycle=" << xml.name();
        qDebug() << "erstr: " << xml.errorString();
        if (xml.name() == "timetable")
            while (xml.readNextStartElement())
            {
                qDebug() << "loadXmlCycle2=" << xml.name();
                if (xml.name() == "classes")
                    readClasses();
                else if (xml.name() == "subjects")
                    readSubjects();
                else if (xml.name() == "rooms")
                    readRooms();
                else if (xml.name() == "chairs")
                    readChairs();
                else if (xml.name() == "teachers")
                {
                    levelRead("teacher", readOneTeacher(););
                }
                else if (xml.name() == "scheds")
                    readScheds();
                else if (xml.name() == "study_types")
                {
                    levelRead("study_type", readStudyType(););
                }
                else if (xml.name() == "loads")
                {
                    levelRead("load", readLoad(););
                }
                else
                    xml.skipCurrentElement();
            }
        else
            xml.skipCurrentElement();
        qDebug() << "erstr: " << xml.errorString();
    }

    if(xml.hasError()){
        throw LogicException(xml.errorString());
    }




}
QList< LoadPart > XMLDataReader::getLoadForGroup(int groupId)
{

    QList<LoadPart> result;

    for (Load& load : getLoads().values())
    {
        if (load.groupsIds.contains(groupId))
            result.append(load.parts);
    }

    return result;
}
QList< LoadPart > XMLDataReader::getLoadForTeacher(int teacher_id)
{

    QList<LoadPart> result;

    for (Load& load : getLoads().values())
    {
        for (const LoadPart& lp : load.parts)
        {
            if (lp.teacher_id == teacher_id)
                result.append(lp);
        }
    }

    return result;
}
QList< LoadPart > XMLDataReader::getLoadForRoom(int room_id)
{

    QList<LoadPart> result;

    for (Load& load : getLoads().values())
    {
        for (const LoadPart& lp : load.parts)
        {
            if (lp.room_id_list.contains(room_id))
                result.append(lp);
        }
    }

    return result;
}
