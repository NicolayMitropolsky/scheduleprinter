#ifndef TEACHERTABLEMODEL_H
#define TEACHERTABLEMODEL_H

#include <QObject>
#include <EntityTableModel.h>

class TeacherTableModel : public EntityTableModel
{
    Q_OBJECT

    QList<QVariant> headers = {
        QVariant::fromValue(QString("Фио")),
        QVariant::fromValue(QString("Кафедра"))
    };

public:
    explicit TeacherTableModel(Semestrator& semestrator, QObject *parent = 0):EntityTableModel(semestrator, parent){}

    QVariant data(const QModelIndex & index, int role) const{

        //qDebug()<<"data called " + QString::number(role)<<" "<<index;

        if(role < Qt::UserRole)
       {
           return QVariant();
       }

       role = role - Qt::UserRole;

        QHash<int, Teacher> const &teachers = semestrator.getData().getTeachers();
        if(teachers.size() == 0)
            return QVariant();

        const Teacher& t = teachers[teachers.keys()[index.row()]];

        if(role == 0)
        return  QVariant::fromValue(t.tname());
        else {
            const Chair& c = semestrator.getData().getChairs()[t.chair_id];
            return  QVariant::fromValue(c.short_name);
        }
    }

    int rowCount(const QModelIndex & parent) const{

        return semestrator.getData().getTeachers().size();

    }

    int columnCount(const QModelIndex & parent) const{
        //qDebug()<<"columnCount called";
        return headers.size();
    }

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const{
        qDebug()<<"headerData called "<<role;
        if(role != Qt::DisplayRole)
            return QVariant();

        return  headers[section];
    }


    int getId(int colnum){
        return semestrator.getData().getTeachers().keys()[colnum];
    }

    QString getEname() {
        return "teacher";
    }


};

#endif // TEACHERTABLEMODEL_H
