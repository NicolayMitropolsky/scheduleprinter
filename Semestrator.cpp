/* 
 * File:   Semestrator.cpp
 * Author: nickl
 *
 * Created on March 27, 2014, 10:48 AM
 */

#include "Semestrator.h"

QList< SemesterTimeTableCell > Semestrator::genTeacher(int teacher_id)
{

    qDebug() << "loading teacher=" << teacher_id;

    auto groupLoads = data.getLoadForTeacher(teacher_id);

    return genTTSForLoadParts(groupLoads);

}
void Semestrator::loadXml(QString name)
{
    data.clearData();
    emit dataChanged();

    try{
        data.loadXml(name);
    }
    catch(const LogicException& e){
        qWarning()<<"Exception:"<<e.message;
        emit error(e.message);
    }

    emit dataChanged();
}
QList< SemesterTimeTableCell > Semestrator::genGroup(int groupId)
{

    qDebug() << "loading group=" << groupId;

    auto groupLoads = data.getLoadForGroup(groupId);

    return genTTSForLoadParts(groupLoads);

}
QList< SemesterTimeTableCell > Semestrator::genRoom(int room_id)
{

    qDebug() << "loading room=" << room_id;

    auto roomLoads = data.getLoadForRoom(room_id);

    return genTTSForLoadParts(roomLoads);

}
QList< SemesterTimeTableCell > Semestrator::genTTSForLoadParts(const QList< LoadPart >& loadParts)
{

    QList<SemesterTimeTableCell> result;


    const QHash<int, QList<Shed> >& sheds = data.getSheds();


    for (const LoadPart& part : loadParts)
    {

        const Load& load = data.getLoads()[part.loadid];

        QList<Shed> loadSheds = sheds[load.id];

        //qDebug() << "load part=" << part;

        QList<Shed> partSheds = filter(loadSheds, [&part](const Shed & i) {
            return i._loadPart == part.loadPartIndex;
        });

        //qDebug() << "partSheds=" << partSheds;

        qSort(partSheds.begin(), partSheds.end(), [](Shed& a, Shed & b) {
            return std::make_tuple(a.begin_date(), a.day(), a.hour()) < std::make_tuple(b.begin_date(), b.day(), b .hour());
        });

        QList<LongShed> longSheds;

        if (canBeGrouped(part))
        {
            longSheds = groupSheds(partSheds);
        }
        else
        {
            longSheds = convert<Shed, LongShed>(partSheds, [](const Shed & shed) {
                return LongShed(shed, shed);
            });
        }

        //qDebug() << "longSheds=" << longSheds;


        QMap < std::tuple<int, int, int>, QList < LongShed >> groupedShedsMap;


        for (const LongShed& shed : longSheds)
        {
            groupedShedsMap[std::make_tuple(shed.day(), shed.hours().first(), shed.room_id())].append(shed);
        }

        auto groupedSheds = groupedShedsMap.values();

        //qDebug() << "gsv=" << gsv;

        auto loadPartCells = convert<QList<LongShed>, SemesterTimeTableCell>(groupedSheds, [&part, this](const QList<LongShed>& shedGroup) {
            //qDebug() << "converting=" << sl;
            return this->extractTimeTableCell(shedGroup, part);
        });

        //qDebug() << "tts=" << tts;

        result.append(loadPartCells);

    }

    return result;

}
bool Semestrator::canBeGrouped(const LoadPart& st) {
    return data.getStudyTypes()[st.study_type_id].full_name.contains("аборат")
            || data.getSubjects()[st.subject_id].full_name.contains("рактика");
}
QList< LongShed > Semestrator::groupSheds(QList< Shed > sheds)
{

    QList<LongShed> result;

    for (int i = 0; i < sheds.size(); i++)
    {
        if (i + 1 >= sheds.size())
            result.append(LongShed(sheds[i], sheds[i]));
        else if (sheds[i].sequental(sheds[i + 1]))
        {
            int j = i + 1;
            while(j+1 < sheds.size() && sheds[j].sequental(sheds[j+1])){
                j++;
            }
            qDebug()<<"i="<<i<<" j="<<j;
            result.append(LongShed(sheds[i], sheds[j]));
            i=j;
        }
        else
            result.append(LongShed(sheds[i], sheds[i]));

    }

    return result;
}
SemesterTimeTableCell Semestrator::extractTimeTableCell(const QList< LongShed >& hourlyGroupedSheds, const LoadPart& loadPart)
{

    int day = hourlyGroupedSheds.first().day();
    auto hours = hourlyGroupedSheds.first().hours();



    QSet<QDate> allDates;

    for (const LongShed& shed : hourlyGroupedSheds)
    {
        Q_ASSERT(shed.day() == day && shed.hours() == hours);


        QDate curDate = shed.begin_date();
        curDate = curDate.addDays(day - curDate.dayOfWeek());

        for (; curDate <= shed.end_date(); curDate = curDate.addDays(7))
        {
            allDates.insert(curDate);
        }

    }

    //qDebug() << "allDates=" << allDates;


    auto ql = allDates.toList();
    qSort(ql);
    QList<Group> grps = convert<int, Group>(data.getLoads()[loadPart.loadid].groupsIds.toList(), [this](int grid) {
        return data.getGroups()[grid];
    });


    return SemesterTimeTableCell(
                day, hours,
                grps,
                data.getSubjects()[loadPart.subject_id],
            data.getTeachers()[loadPart.teacher_id],
            data.getStudyTypes()[loadPart.study_type_id],
            loadPart,
            data.getRooms()[hourlyGroupedSheds.first().room_id()],
            ql
            );


}
