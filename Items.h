

#ifndef ITEMS_H
#define	ITEMS_H

#include <Qt>
#include <QDebug>
#include <QString>
#include <QDate>
#include <set>

struct Group
{
    int id;
    QString name;
    int semestr;

    Group(int id, QString name, int semestr) : id(id), name(name), semestr(semestr)
    {
    }

    Group()
    {
    }

};

QDebug &operator<<(QDebug &stream, const Group& t);

struct Subject
{
    int id;
    QString short_name;
    QString full_name;

    Subject(int id, QString short_name, QString full_name)
    : id(id), short_name(short_name), full_name(full_name)
    {
    }

    Subject()
    {
    }

};

QDebug &operator<<(QDebug &stream, const Subject& t);

struct Room
{
    int id;
    QString name;
    int capacity;

    Room(int id, QString name, int capacity) :
        id(id),
        name(name),
        capacity(capacity)
    {
    }

    Room()
    {
    }

};

QDebug &operator<<(QDebug &stream, const Room& t);

struct Teacher
{
    int id;
    QString surname;
    QString first_name;
    QString second_name;
    int chair_id;

    Teacher(int id, QString surname, QString first_name, QString second_name, int chair_id)
    : id(id), surname(surname), first_name(first_name), second_name(second_name), chair_id(chair_id)
    {
    }

    Teacher()
    {
    }

    QString tname() const
    {
        //return (surname+" "+first_name+" "+second_name).simplified();
        return surname + " " + initial(first_name) + initial(second_name);
    }

    static QString initial(const QString& full, QString optapp = "")
    {
        if (full.isEmpty())
            return "";
        else
            return QString(full[0]) + "." + optapp;
    }

};

QDebug &operator<<(QDebug &stream, const Teacher& t);

struct Chair
{
    int id;
    QString short_name;
    QString full_name;

    Chair(int id, QString short_name, QString full_name)
    : id(id), short_name(short_name), full_name(full_name)
    {
    }

    Chair()
    {
    }

};

QDebug &operator<<(QDebug &stream, const Chair& t);

//struct AbstractShed
//{
//    virtual int day() const = 0;
//
//    virtual QList<int> hours() const = 0;
//
//    virtual int loadPart() const = 0;
//
//    virtual int room_id() const = 0;
//
//    virtual int load_id() const = 0;
//
//    virtual QDate begin_date() const = 0;
//
//    virtual QDate end_date() const = 0;
//
//    virtual ~AbstractShed()
//    {
//    }
//};

struct Shed //: public AbstractShed
{
    int _day;
    int _hour;
    int _loadPart;
    int _room_id;
    int _load_id;
    QDate _begin_date;
    QDate _end_date;

    Shed(int day,
            int hour,
            int loadPart,
            int room_id,
            int load_id,
            QDate begin_date,
            QDate end_date)
    : _day(day), _hour(hour), _loadPart(loadPart), _room_id(room_id), _load_id(load_id),
    _begin_date(begin_date), _end_date(end_date)

    {
    }

    Shed()
    {
    }

    int day() const
    {
        return _day;
    }

    int hour() const
    {
        return _hour;
    }

    QList<int> hours() const
    {
        return QList<int>({_hour});
    }

    int loadPart() const
    {
        return _loadPart;
    }

    int room_id() const
    {
        return _room_id;
    }

    int load_id() const
    {
        return _load_id;
    }

    QDate begin_date() const
    {
        return _begin_date;
    }

    QDate end_date() const
    {
        return _end_date;
    }

    bool inOneDay(const Shed& another)
    {
        return begin_date() == another.begin_date() && day() == another.day() && end_date() == another.end_date();
    }

    bool sequental(const Shed& another)
    {
        return inOneDay(another) && hour() == another.hour() - 1;
    }

};

QDebug &operator<<(QDebug &stream, const Shed& t);

class LongShed// : public AbstractShed
{
    Shed first;
    Shed last;

public:

    LongShed(const Shed& first, const Shed& last) : first(first), last(last)
    {
        Q_ASSERT(first.day() == last.day());
        //Q_ASSERT(first.hour() == last.hour() - 1 || first.hour() == last.hour());
        Q_ASSERT(first.loadPart() == last.loadPart());
        Q_ASSERT(first.room_id() == last.room_id());
        Q_ASSERT(first.begin_date() == last.begin_date());
        Q_ASSERT(first.end_date() == last.end_date());
    }

    int day() const
    {
        return first.day();
    }

    QList<int> hours() const
    {
        //std::set<int> s({first._hour, last._hour});
        std::set<int> s;

        for(int i = first._hour; i <= last._hour; ++i){
            s.insert(i);
        }

        QList<int> l;
        for (int e : s)
            l.append(e);
        return l;
    }

    int loadPart() const
    {
        return first.loadPart();
    }

    int room_id() const
    {
        return first.room_id();
    }

    int load_id() const
    {
        return first.load_id();
    }

    QDate begin_date() const
    {
        return first.begin_date();
    }

    QDate end_date() const
    {
        return first._end_date;
    }


};

QDebug &operator<<(QDebug &stream, const LongShed& t);

struct StudyType
{
    int id;
    QString short_name;
    QString full_name;

    StudyType(int id, QString short_name, QString full_name)
    : id(id), short_name(short_name), full_name(full_name)
    {
    }

    StudyType()
    {
    }

};

QDebug &operator<<(QDebug &stream, const StudyType& t);

struct LoadPart
{
    int loadid = -1;
    int loadPartIndex = -1;
    int loadPartsCount = -1;
    int teacher_id;
    int subject_id;
    int study_type_id;
    QList<int> room_id_list;
    QList<int> hour_per_week_list;

    LoadPart(int teacher_id, int subject_id, int study_type_id, QList<int> room_id_list, QList<int> hour_per_week_list)
    :
    teacher_id(teacher_id),
    subject_id(subject_id),
    study_type_id(study_type_id),
    room_id_list(room_id_list),
    hour_per_week_list(hour_per_week_list)
    {
    }

    LoadPart()
    {
    }

};

QDebug &operator<<(QDebug &stream, const LoadPart& t);

struct Load
{
    int id;
    QList<LoadPart> parts;
    QSet<int> groupsIds;

    Load(int id, QList<LoadPart> parts, QList<int> groupsIds)
    : id(id), parts(parts), groupsIds(groupsIds.toSet())
    {
    }

    Load()
    {
    }

};

QDebug &operator<<(QDebug &stream, const Load& t);

/*
 
 class TimeTableCell(
                     val day: Int,
                     val hours: Range,
                     val subj: Subject,
                     val teacher: Teacher,
                     val stype: StudyType,
                     val loadPart: LoadPart,
                     val place: Room,
                     val dates: IndexedSeq[DateTime]
                     ) extends Serializable {

  override def toString = s"TimeTableCell(day=$day,hours=$hours,subj=$subj,teacher=$teacher,stype=$stype,loadPart=$loadPart,place=$place,dates=$dates)"

}
 
 */

struct SemesterTimeTableCell
{
    int day;
    QList<int> hours;
    QList<Group> groups;  
    Subject subject;
    Teacher teacher;
    StudyType studyType;
    LoadPart loadPart;
    Room place;
    QList<QDate> dates;

    SemesterTimeTableCell(int day,
            QList<int> hours,
            QList<Group> groups,
            Subject subject,
            Teacher teacher,
            StudyType studyType,
            LoadPart loadPart,
            Room place,
            QList<QDate> dates) : day(day), hours(hours), groups(groups), subject(subject), teacher(teacher), studyType(studyType), loadPart(loadPart), place(place), dates(dates)
    {

    }

    SemesterTimeTableCell()
    {
    }



};

QDebug &operator<<(QDebug &stream, const SemesterTimeTableCell& t);


#endif	/* ITEMS_H */

