/* 
 * File:   Semestrator.h
 * Author: nickl
 *
 * Created on March 27, 2014, 10:48 AM
 */

#ifndef SEMESTRATOR_H
#define	SEMESTRATOR_H

#include <Qt>
#include <functional>
#include <QDebug>
#include <QtCore/qlogging.h>
#include <QApplication>
#include <QXmlStreamReader>
#include <QFile>
#include <QDate>
#include <QUrl>
#include "Items.h"
#include "XMLDataReader.h"
#include <QtAlgorithms>
#include "utils.h"
#include "QPainterExporter.h"

class Semestrator: public QObject{
    Q_OBJECT

    XMLDataReader data;

public:

    Semestrator(QObject* parent = 0) : QObject(parent) {}

    Q_INVOKABLE void loadXml(QString name);

    Q_INVOKABLE QString urlToFilePath(QUrl url)
    {
        return url.toLocalFile();
    }

    QList<SemesterTimeTableCell> genGroup(int groupId);

    QList<SemesterTimeTableCell> genTeacher(int teacher_id);

    QList<SemesterTimeTableCell> genRoom(int room_id);



private:

    QList<SemesterTimeTableCell> genTTSForLoadParts(const QList<LoadPart>& loadParts);

    bool canBeGrouped(const LoadPart& st);

    QList<LongShed> groupSheds(QList<Shed> sheds);    


    SemesterTimeTableCell extractTimeTableCell(const QList<LongShed>& hourlyGroupedSheds, const LoadPart& loadPart);

public:

    const XMLDataReader& getData() const
    {
        return data;
    }

signals:

    void dataChanged();
    void error(QString message);


};

#endif	/* SEMESTRATOR_H */

