#-------------------------------------------------
#
# Project created by QtCreator 2014-03-27T21:03:11
#
#-------------------------------------------------

folder_01.source = qml/SchedulePrinterUI
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

QT = core gui widgets printsupport

QML_IMPORT_PATH =

TARGET = SchedulePrinter
#CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Items.cpp \
    Semestrator.cpp \
    XMLDataReader.cpp \
    HtmlExport.cpp \
    TTCPrinter.cpp \
    main0.cpp \
    ExporterManager.cpp \
    LogicException.cpp \
    ExtendedQSortFilterProxyModel.cpp \
    EntityTableModel.cpp \
    GroupTableModel.cpp \
    TeacherTableModel.cpp \
    RoomTableModel.cpp \
    Settings.cpp \
    MultipleRoomsExporter.cpp \
    RoomsExtendedExporter.cpp \
    QPainterExporter.cpp \
    ExamExporter.cpp

HEADERS += \
    Items.h \
    Semestrator.h \
    utils.h \
    XMLDataReader.h \
    HtmlExport.h \
    BaseExporter.h \
    TTCPrinter.h \
    ExporterManager.h \
    LogicException.h \
    ExtendedQSortFilterProxyModel.h \
    EntityTableModel.h \
    GroupTableModel.h \
    TeacherTableModel.h \
    RoomTableModel.h \
    Settings.h \
    MultipleRoomsExporter.h \
    RoomsExtendedExporter.h \
    QPainterExporter.h \
    ExamExporter.h

equals(QT_MAJOR_VERSION, 4) {
QMAKE_CXXFLAGS += -std=c++11
}
equals(QT_MAJOR_VERSION, 5) {
CONFIG += c++11
}

# Please do not modify the following two lines. Required for deployment.
include(qtquick2controlsapplicationviewer/qtquick2controlsapplicationviewer.pri)
qtcAddDeployment()
