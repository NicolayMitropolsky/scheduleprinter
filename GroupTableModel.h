#ifndef GROUPTABLEMODEL_H
#define GROUPTABLEMODEL_H

#include <QObject>
#include <EntityTableModel.h>

class GroupTableModel : public EntityTableModel
{
    Q_OBJECT
public:
    explicit GroupTableModel(Semestrator& semestrator, QObject *parent = 0):EntityTableModel(semestrator, parent){}

    QList<QVariant> headers = {
        QVariant::fromValue(QString("Название")),
        QVariant::fromValue(QString("Семестр"))
    };

    QVariant data(const QModelIndex & index, int role) const{

//        qDebug()<<"GroupTableModel data called role" + QString::number(role);
//        qDebug()<<"GroupTableModel data called index"<<index;
        if(role < Qt::UserRole)
        {
            return QVariant();
        }

        role = role - Qt::UserRole;

        auto groups = semestrator.getData().getGroups();
        if(groups.size() == 0)
            return QVariant();

        Group gr = groups[groups.keys()[index.row()]];

        return role==0?
                    QVariant::fromValue(gr.name):
                    QVariant::fromValue(QString("Семестр")+ QString::number(gr.semestr))
                    ;
    }

    int rowCount(const QModelIndex & parent) const{

        return semestrator.getData().getGroups().size();

    }

    int columnCount(const QModelIndex & parent) const{
        //qDebug()<<"columnCount called";
        return headers.size();
    }

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const{
        qDebug()<<"headerData called "<<role;
        if(role != Qt::DisplayRole)
            return QVariant();

        return  headers[section];
    }

    int getId(int colnum){
        return semestrator.getData().getGroups().keys()[colnum];
    }

    QString getEname() {
        return "group";
    }


};

#endif // GROUPTABLEMODEL_H
