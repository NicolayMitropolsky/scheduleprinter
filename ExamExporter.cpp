#include "ExamExporter.h"

QDebug &operator<<(QDebug &stream, const ExamSubject& t)
{
    stream << "ExamSubject(" << t.subject << " exams=" << t.exam << ", consultation=" << t.consultation << ")";
    return stream;
}