/* 
 * File:   TTCPrinter.h
 * Author: nickl
 *
 * Created on May 10, 2014, 9:20 AM
 */

#ifndef TTCPRINTER_H
#define	TTCPRINTER_H

#include <Qt>
#include <QDebug>
#include <QtCore/qlogging.h>
#include <QApplication>
#include <QPrinter>
#include <QPen>
#include <QPainter>
#include "Items.h"
#include <cmath>

class TTCPrinter
{
public:

    bool printGroup = true;
    bool printTeacher = true;
    bool printKlass = true;

    TTCPrinter(bool printGroup, bool printTeacher, bool printKlass) :
    printGroup(printGroup), printTeacher(printTeacher), printKlass(printKlass)
    {
    }

    TTCPrinter()
    {
    }

    QString printTTC(const SemesterTimeTableCell& tt);

    QString printTTCs(QList<SemesterTimeTableCell> ttcs);

private:

    QString datesString(const QList<QDate>& dates);

    QList<std::pair<int, int >> detectRepeation(QList<int> data, int l = 3);

};

#endif	/* TTCPRINTER_H */

