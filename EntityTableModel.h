#ifndef CLASSESTABLEMODEL_H
#define CLASSESTABLEMODEL_H

#include <QAbstractTableModel>
#include "Semestrator.h"


class EntityTableModel : public QAbstractTableModel
{
    Q_OBJECT

    Q_PROPERTY(QString ename READ getEname)

protected:

    Semestrator& semestrator;

    QHash<int, QByteArray> roles = {
        {Qt::UserRole+0, "column0"},
        {Qt::UserRole+1, "column1"},
        {Qt::UserRole+2, "column2"},
        {Qt::UserRole+3, "column3"},
        {Qt::UserRole+4, "column4"},
        {Qt::UserRole+5, "column5"}
    };



public:
    explicit EntityTableModel(Semestrator& semestrator, QObject *parent = 0);

//    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

//    int rowCount(const QModelIndex & parent = QModelIndex()) const;

//    int columnCount(const QModelIndex & parent = QModelIndex()) const;

    Q_INVOKABLE virtual int getId(int colnum) = 0;

    virtual QString getEname() = 0;

    QHash<int, QByteArray> roleNames() const{
        return roles;
    }


private slots:

    void invokeRefresh();

};

#endif // CLASSESTABLEMODEL_H
