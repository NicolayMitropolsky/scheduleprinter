/* 
 * File:   utils.h
 * Author: nickl
 *
 * Created on March 27, 2014, 10:47 AM
 */

#ifndef UTILS_H
#define	UTILS_H

#include <QList>

template<class T, class F>
QList<T> filter(QList<T>& orig, F func)
{
    QList<T> result;
    for (T& o : orig)
    {
        if (func(o))
            result.append(o);
    }
    return result;

}

template<class K, class T>
QList<T> convert(const QList<K>& orig, std::function<T(const K&)> func)
{
    QList<T> result;
    for (const K& o : orig)
    {
        result.append(func(o));
    }
    return result;

}

template<class T>
bool containsAny(const QSet<T>& set, const QList<T>& elems)
{
    for (const T& e : elems)
    {
        if (set.contains(e))
            return true;
    }

    return false;
}

template<class T, class F>
bool exists(const QList<T>& elems, F predicate)
{
    for (const T& e : elems)
    {
        if (predicate(e))
            return true;
    }

    return false;
}

#endif	/* UTILS_H */

