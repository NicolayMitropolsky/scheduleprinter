

#include "Semestrator.h"
#include "HtmlExport.h"
#include "QPainterExporter.h"
#include "ExporterManager.h"
#include <QStringList>
#include <QDir>


void runPdfExport(QPainterExporter& pdfexp, QString filename)
{
    QPrinter printer(QPrinter::HighResolution);
    //printer.setResolution(1200);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOrientation(QPrinter::Landscape);
    qDebug()<<"writing filename:"<<filename;
    printer.setOutputFileName(filename);

    pdfexp.runExport(printer);
}

int main0(int argc, char *argv[])
{

    QApplication app(argc, argv);

    //    XMLDataReader r;
    //    r.loadXml("/home/nickl/Yandex.Disk/Расписание МГТУ СТАНКИН/Расписание Весна 2014.xml");
    //
    //    //    auto gr = r.getGroups();
    //    qDebug() << "got gr";
    //    //
    //    //    qDebug() << "groups=" << gr;
    //    qDebug() << "loads=";
    //
    //    for (auto l : r.getLoads())
    //    {
    //        qDebug() << " " << l;
    //    }


    Semestrator semestrator;

    QStringList args = QApplication::arguments();

    if (args.length() < 2)
        semestrator.loadXml("/home/nickl/Yandex.Disk/Расписание МГТУ СТАНКИН/Экзамены Зима 2015 Итоговое расписание.xml");
    else
        semestrator.loadXml(args.at(1));


    ExporterManager exporterManager(semestrator);

    exporterManager.write("teacher", QList<int>()<<396<<266);


//    QDir::current().mkdir("groups");
//
//    //for (int groupId : semestrator.getData().getGroups().keys())
//    //{
//        int groupId = 108;
//
//        QList<SemesterTimeTableCell> grouptts = semestrator.genGroup(groupId);
//
//        QString groupName = semestrator.getData().getGroups()[groupId].name;
//        TTCPrinter pr(false, true, true);
//        HtmlExport exp(pr);
//        QPainterExporter pdfexp(pr);
//
//        exp.setTitle(groupName);
//        pdfexp.setTitle(groupName);
//
//        exp.setData(grouptts);
//        pdfexp.setData(grouptts);
//
//        QFile file("groups/" + groupName + ".html");
//
//        file.open(QFile::WriteOnly);
//
//        exp.write(file);
//        runPdfExport(pdfexp, "groups/" + groupName+"-"+ QString::number(groupId) + ".pdf");
//
//        file.close();
//    //}

//    QDir::current().mkdir("teachers");

//    for (int teacherId : semestrator.getData().getTeachers().keys())
//    {
//        //int groupId = 19;

//        QList<SemesterTimeTableCell> tts = semestrator.genTeacher(teacherId);

//        QString teacherName = semestrator.getData().getTeachers()[teacherId].tname();

//        TTCPrinter pr(true, false, true);
//        PDFExporter pdfexp(pr);

//        pdfexp.setTitle(teacherName);

//        pdfexp.setData(tts);

//        runPdfExport(pdfexp, "teachers/" + teacherName.remove(".") + "-" + QString::number(teacherId) + ".pdf");

//    }

//    QDir::current().mkdir("rooms");

//    for (int room_id : semestrator.getData().getRooms().keys())
//    {
//        //int groupId = 19;

//        QList<SemesterTimeTableCell> tts = semestrator.genRoom(room_id);

//        QString roomName = semestrator.getData().getRooms()[room_id].name;

//        TTCPrinter pr(true, true, false);
//        PDFExporter pdfexp(pr);

//        pdfexp.setTitle(roomName);

//        pdfexp.setData(tts);

//        QString fname = "rooms/" + roomName.remove(".").remove("/").remove("(").remove(")").remove("\"")+ "-" + QString::number(room_id)  + ".pdf";

//        qDebug()<<"fname="<<fname;

//        runPdfExport(pdfexp, fname);

//    }


    //return app.exec();
}
