#ifndef ROOMSWEEKLYPDFEXPORTER_H
#define ROOMSWEEKLYPDFEXPORTER_H
#include "QPainterExporter.h"

class MultipleRoomsExporter : public QPainterExporter
{

protected:

    QHash<int, QHash<int, QHash<int, QList<SemesterTimeTableCell >>>> timetableByDay;

    QDate from = QDate(2014, 2 ,10);

    QDate to = from.addDays(7);

    QList<Room> rooms;

    float v = 13;

    int hx = 30 * v;
    int hy = 30 * v;

    int roomNameswidth = 100 * v;

    virtual int pointCellWidth() {
        return 30 * v;
    }

    virtual int cellheight() {
        return 30 * v;
    }

    int headerHeight = 100 * v;
    int subheader = 30 * v;
    int margin = 4 * v;

    int dayWidth() {
        return pointCellWidth() *hoursIndxs.size();
    }

    int maxWidth = 9000;

    int daysInPage() {
        return (maxWidth - roomNameswidth) / dayWidth();
    }

    int maxHeight = 12000;

    int roomsInPage(){
        return (maxHeight - headerHeight) / cellheight();
    }

    void reinit(){
        fromPage = 0;
        toPage = 0;
    }


public:
    MultipleRoomsExporter(TTCPrinter ttc):QPainterExporter(ttc)
    {
    }

    int fromPage = 0;
    int toPage = 0;

    virtual QPrinter::Orientation orientationHint(){
        return QPrinter::Portrait;
    }

    void setData(const QList<SemesterTimeTableCell>& data) override;

    void setRooms(const QList<Room>& rooms){
        reinit();
        this->rooms = rooms;
    }

    void setInterval(const QDate& from, const QDate& to){
        reinit();
        this->from = from;
        this->to = to;
    }

    virtual bool isWeekDependent(){
        return true;
    }

    void runExport(QPrinter& printer, QPainter& painter) override;

protected:

    QList< QList<Room> > groupRooms(QList<Room> rooms, int size);

    void writeRooms(QPainter& painter, QList<Room> rooms);

    void drawBoundary(QPainter& painter,int printedDays, int roomsSize);

    virtual QString headingDateFormat(){
        return "dd.MM.yy dddd";
    }

    virtual void drawTimeHeader(int dx, int dy, QPainter& painter, int j);

    void writeDay(QPainter& painter, QList<Room> rooms, QDate curdate, int dx, int dy);

    virtual void paintCell(QPainter& painter,
                           int x1, int y1,
                           const QDate& curdate,
                           const QList<SemesterTimeTableCell >& celldata);

    virtual bool containsDate(const QList<SemesterTimeTableCell > & celldata, const QDate& date);


};

#endif // ROOMSWEEKLYPDFEXPORTER_H
