#ifndef ROOMSWEEKLYEXTENDEDEXPORTER_H
#define ROOMSWEEKLYEXTENDEDEXPORTER_H

#include "MultipleRoomsExporter.h"

class RoomsExtendedExporter : public MultipleRoomsExporter
{
protected:
    virtual int pointCellWidth() override {
       return 70 * v;
    }


    virtual int cellheight() override {
       return 120 * v;
    }

    virtual void paintCell(QPainter& painter,
                           int x1, int y1,
                           const QDate& curdate,
                           const QList<SemesterTimeTableCell >& celldata) override
    {
        QRectF br1(x1 + margin,
                         y1 + margin,
                         pointCellWidth() - margin * 2,
                         cellheight() - margin * 2
                         );

       QString text = ttcPrinter.printTTCs(celldata);

       drawFitToRect(painter, br1, text);

    }

    virtual bool isWeekDependent() override {
        return false;
    }

    virtual bool containsDate(const QList<SemesterTimeTableCell > & celldata, const QDate& date) override  {
       return true; //Расширенный список содержит информацию по всем занятиям в этот день в течении семестра.
    }

    virtual QString headingDateFormat() override{
        return "dddd";
    }

    virtual void drawTimeHeader(int dx, int dy, QPainter& painter, int j) override {

        painter.drawText(dx+ pointCellWidth() * j + margin, dy + subheader +margin,
                         pointCellWidth()-margin, headerHeight - subheader -margin,
                         Qt::AlignVCenter | Qt::AlignCenter,
                         hours[j]);

    }



public:  

    RoomsExtendedExporter(TTCPrinter ttc):MultipleRoomsExporter(ttc)
    {
        headerHeight = 60 * v;
    }


    /*
                QRectF br1(verts[i + 1] + padding, hors[day] + padding,
                        verts[i + 1 + hl] - verts[i + 1] - padding * 2, midH - hors[day] - padding * 2);

                drawFitToRect(painter, br1, firstCellText);
     */
};

#endif // ROOMSWEEKLYEXTENDEDEXPORTER_H
