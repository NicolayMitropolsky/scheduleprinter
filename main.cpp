#include "qtquick2controlsapplicationviewer.h"
#include <QQmlContext>
#include <QtQml>
#include "GroupTableModel.h"
#include "TeacherTableModel.h"
#include "Semestrator.h"
#include "ExporterManager.h"
#include "ExtendedQSortFilterProxyModel.h"
#include "RoomTableModel.h"
#include "Settings.h"

int main(int argc, char *argv[])
{

    Application app(argc, argv);
    app.setOrganizationName("Stankin");
    app.setApplicationName("SchedulePrinter");

    //qmlRegisterUncreatableType<ExporterManager>("Stankin.SchedulePrinter", 1, 0, "ExporterManager","ExporterManager cant be created");

    QtQuick2ControlsApplicationViewer viewer;
    Settings settings;


    Semestrator semestrator;

    ExporterManager exporterManager(semestrator);

    GroupTableModel gtb(semestrator);
    TeacherTableModel ttb(semestrator);
    RoomTableModel rtb(semestrator);

    ExtendedQSortFilterProxyModel proxyModel;
    //proxyModel.sort(0);
    //proxyModel.setSourceModel(&gtb);

    QQmlContext *ctxt = viewer.rootContext();
    ctxt->setContextProperty("settings", &settings);
    ctxt->setContextProperty("gropsTM", &gtb);
    ctxt->setContextProperty("theacherTM", &ttb);
    ctxt->setContextProperty("roomTM", &rtb);
    ctxt->setContextProperty("proxyModel", &proxyModel);
    ctxt->setContextProperty("semestrator", &semestrator);
    ctxt->setContextProperty("exporterManager", &exporterManager);
    viewer.setMainQmlFile(QStringLiteral("qml/SchedulePrinterUI/main.qml"));
    viewer.show();

    return app.exec();
}
