# Печать расписаний МГТУ "Станкин" #

Утилита для печати расписаний занятий МГТУ "Станкин", составленных в программе [Ректор-Вуз](http://rector.spb.ru/raspisanie-vuz-4u.php), в привычном для Станкина семестровом виде.

## Вы можете ##

* [Скачать последнюю версию программы](https://bitbucket.org/NicolayMitropolsky/scheduleprinter/downloads)
* [Посмотреть и скачать исходные коды программы](https://bitbucket.org/NicolayMitropolsky/scheduleprinter/src)
* [Сообщить об ошибке или высказать пожелания по усовершенствованию программы](https://bitbucket.org/NicolayMitropolsky/scheduleprinter/issues)

## Лицензия ##

Программа распространяется в соответствии с лицензией [GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.html)