/* 
 * File:   BaseExporter.h
 * Author: nickl
 *
 * Created on April 11, 2014, 6:48 PM
 */

#ifndef EXPORTERBASE_H
#define	EXPORTERBASE_H

#include "Items.h"
#include "utils.h"
#include <QIODevice>
#include <QStringList>
#include <cmath>
#include "TTCPrinter.h"

class BaseExporter{
  
protected:
    
    TTCPrinter ttcPrinter;

    QString title;

    QHash<int, QHash<int, QList<SemesterTimeTableCell >> > timetableByDay;

    QList<QString> daynames = {"пн.", "вт.", "ср.", "чт.", "пт.", "сб."};
    
    QList<QString> hours = {
        "8:30 - 10:10",
        "10:20 - 12:00",
        "12:20 - 14:00",
        "14:10 - 15:50",
        "16:00 - 17:40",
        "18:00 - 19:30",
        "19:40 - 21:10",
        "21:20 - 22:50"
    };

    QList<QString> daynamesFull = {
        "Понедельник",
        "Вторник",
        "Среда",
        "Четверг",
        "Пятница",
        "Суббота"
    };

    QList<int> hoursIndxs = {1, 2, 3, 4, 5, 6, 7, 8};
    
    QString printTTCs(QList<SemesterTimeTableCell> ttcs)
    {
       return ttcPrinter.printTTCs(ttcs);
    }
    
    QSet<int> retriveComplexHours(const QHash<int, QList<SemesterTimeTableCell >> &dayttcs)
    {

        QSet<int> result;
        const QList<QList<SemesterTimeTableCell>> values = dayttcs.values();

        for (const QList<SemesterTimeTableCell>& qltts : values)
        {
            int minHourLen = INT32_MAX;
            QList<int> hours;

            for (const SemesterTimeTableCell& tts : qltts)
            {
                if (tts.hours.size() < minHourLen)
                {
                    minHourLen = tts.hours.size();
                    hours = tts.hours;
                }
            }

            //qDebug()<<"hors="<<hours;

            for (int h : hours )
            for (const SemesterTimeTableCell& tts : dayttcs[h])
            {
                if (tts.hours.size() != minHourLen)
                {
                    //qDebug()<<"tts.hours="<<tts.hours;
                    result += tts.hours.toSet();
                    result += hours.toSet();
                }
            }
        }

        return result;

    }
    
    
public:
    
    BaseExporter(TTCPrinter ttcPrinter) :
    ttcPrinter(ttcPrinter)
    {
    }


    BaseExporter()
    {
    }

    void setTitle(QString title)
    {
        this->title = title;
    }

    virtual void setData(const QList<SemesterTimeTableCell>& data)
    {
        
        //qDebug() << "setData arg=" << data;
        
        timetableByDay.clear();

        for (const SemesterTimeTableCell& cell : data)
        {
            timetableByDay[cell.day][cell.hours.first()].append(cell);
        }
    }


    TTCPrinter const &getTtcPrinter() const
    {
        return ttcPrinter;
    }

    void setTtcPrinter(TTCPrinter const &ttcPrinter)
    {
        BaseExporter::ttcPrinter = ttcPrinter;
    }
};


#endif	/* EXPORTERBASE_H */

