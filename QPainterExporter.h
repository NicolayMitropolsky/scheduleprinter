

#ifndef PDFEXPORTER_H
#define	PDFEXPORTER_H

#include <Qt>
#include <QDebug>
#include <QtCore/qlogging.h>
#include <QApplication>
#include <QPrinter>
#include <QPen>
#include <QPainter>
#include "Items.h"
#include "BaseExporter.h"
#include <cmath>
#include <QPrintPreviewDialog>

class QPainterExporter : public BaseExporter
{
protected:

    QString fontName = "Arial";

    QFont getFont(int size){
        QFont f(fontName, size);
//        f.setStyleStrategy(QFont::ForceOutline);
//        f.setKerning(false);
//        f.setFixedPitch(false);
//        f.setStyleHint(QFont::SansSerif);
//        //f.setLetterSpacing(QFont::AbsoluteSpacing, 1);
//        f.setHintingPreference (QFont::PreferNoHinting );
        return f;
    }

    float v = 13;

    virtual int smallFont(){
        return v * 0.65;
    }

    virtual int bigFont(){
        return v * 1.1;
    }



    void drawRotatedText(QPainter &painter, int x, int y, int width, int height, const QString &text);

    QList<int> verts;

    QList<int> hors;

    //QPainter painter;

    int padding = 20;
    

    void adaptFontSize(QPainter& painter, int flags, QRectF rect, QString text, QFont font);

    int maxLen(const QList<SemesterTimeTableCell> & ttsl);

    void writeDay(QPainter& painter,int day);

    void drawFitToRect(QPainter& painter,QRectF br, QString text);


public:
    
    QPainterExporter(TTCPrinter ttcPrinter) :
    BaseExporter(ttcPrinter)
    {
    }

    QPainterExporter() : BaseExporter()
    {
    }

    virtual QPrinter::Orientation orientationHint(){
        return QPrinter::Landscape;
    }

    void runExport(QPrinter& printer);

    virtual void runExport(QPrinter& printer, QPainter& painter);

private:
    int getHorDivider(int day, int ceilSize, int floorsize);

    void drawInOne(QPainter &painter, int day, int i, QList<SemesterTimeTableCell> &ds, int hl);
};

#endif	/* PDFEXPORTER_H */

