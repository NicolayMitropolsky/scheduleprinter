/* 
 * File:   HtmlExport.h
 * Author: nickl
 *
 * Created on March 29, 2014, 4:44 PM
 */

#ifndef HTMLEXPORT_H
#define	HTMLEXPORT_H

#include "Items.h"
#include "utils.h"
#include <QIODevice>
#include <QStringList>
#include <cmath>
#include "BaseExporter.h"



class HtmlExport: public BaseExporter
{


    void writeDay(QTextStream& out, int day)
    {
        out << "<tr>" << endl;

        auto dayttcs = timetableByDay[day];
        //qDebug() << "dayttcs =" << dayttcs;
        auto complexHours = retriveComplexHours(dayttcs);
        //qDebug() << "complexHours =" << complexHours;

        QList < QList < SemesterTimeTableCell >> firstFloor = QVector < QList < SemesterTimeTableCell >> (hoursIndxs.size()).toList();

        for (int i = 0; i < hoursIndxs.size(); i++)
        {
            int di = hoursIndxs[i];

            for (const SemesterTimeTableCell& ttc : dayttcs[di])
            {
                if (!containsAny(complexHours, ttc.hours) || ttc.hours.size() > 1)
                    firstFloor[i].append(ttc);
            }

        }

        QList < QList < SemesterTimeTableCell >> secondFloor = QVector < QList < SemesterTimeTableCell >> (hoursIndxs.size()).toList();

        for (int i = 0; i < hoursIndxs.size(); i++)
        {
            int di = hoursIndxs[i];

            for (const SemesterTimeTableCell& ttc : dayttcs[di])
            {
                if (containsAny(complexHours, ttc.hours) && ttc.hours.size() == 1)
                    secondFloor[i].append(ttc);
            }

        }


        int defaultRowSpan = 1;

        if (complexHours.size() > 0)
            defaultRowSpan = 2;

        out << "<td class='daynamecell' rowspan='" << defaultRowSpan << "'>" << daynames[day - 1] << "</td>" << endl;

        for (int i = 0; i < firstFloor.size(); i++)
        {
            QList<SemesterTimeTableCell> ttcs = firstFloor[i];
            if (ttcs.size() == 0)
            {
                out << "<td width='30' rowspan='" << defaultRowSpan << "'></td>" << endl;
            }
            else
            {
                int rs = exists(ttcs, [&complexHours](const SemesterTimeTableCell & ttc) {
                    return containsAny(complexHours, ttc.hours);
                }) ? 1 : defaultRowSpan;

                int cs = ttcs.first().hours.size();
                i += cs - 1;

                out << "<td rowspan='" << rs << "' colspan='" << cs << "' >" << endl;
                out << printTTCs(ttcs) << endl;
                out << "</td>" << endl;

            }

        }

        if (secondFloor.size() > 0)
        {

            out << "</tr>" << endl;
            out << "<tr>" << endl;

            for (int i = 0; i < secondFloor.size(); i++)
            {
                QList<SemesterTimeTableCell> ttcs = secondFloor[i];
                if (ttcs.size() == 0)
                {
                    if (complexHours.contains(hoursIndxs[i]))
                        out << "<td width='30' rowspan='1'></td>" << endl;
                }
                else
                {
                    int rs = exists(ttcs, [&complexHours](const SemesterTimeTableCell & ttc) {
                        return containsAny(complexHours, ttc.hours);
                    }) ? 1 : defaultRowSpan;

                    int cs = ttcs.first().hours.size();

                    out << "<td rowspan='" << rs << "' colspan='" << cs << "' >" << endl;
                    out << printTTCs(ttcs) << endl;
                    out << "</td>" << endl;

                }

            }


        }

        out << "</tr>" << endl;

    }

    

    //    QString datesString(const QList<QDate>& dates)
    //    {
    //
    //        QStringList res;
    //
    //        for (const QDate& date : dates)
    //        {
    //            res.append(date.toString("dd.MM"));
    //        }
    //
    //        return res.join(", ");
    //    }

    
 

    QString printTTCs(const QList<SemesterTimeTableCell>& ttcs)
    {
        return BaseExporter::ttcPrinter.printTTCs(ttcs).replace("\n","<br/>");
    }   


public:

    HtmlExport(TTCPrinter ttcPrinter) :
    BaseExporter(ttcPrinter)
    {
    }


    void write(QIODevice& outdev)
    {

        QTextStream out(&outdev);
        out.setCodec("UTF-8");

        out << "<html>" << endl;
        out << "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\">" << endl;
        //out << "<link rel="stylesheet" href="style.css">";
        out << "<style>" << endl;
        out << "        table{" << endl;
        out << "      border-collapse : collapse;" << endl;
        out << "      font-size : 11pt;" << endl;
        out << "      font-family : Lucida Grande, Arial, tahoma, verdana, sans-serif;" << endl;
        out << "     " << endl;
        out << "}" << endl;
        out << " td, th{" << endl;
        out << "      border : 1px solid black;" << endl;
        out << "      padding : 3px 3px 3px 3px;" << endl;
        out << "      vertical-align : top;" << endl;
        out << "      min-height : 100px;" << endl;
        out << "      min-width : 120px;" << endl;
        out << "     " << endl;
        out << "}" << endl;
        out << " td.nameheader{" << endl;
        out << "      height : 10px;" << endl;
        out << "     " << endl;
        out << "}" << endl;
        out << " td.daynamecell{" << endl;
        out << "      font-weight : bold;" << endl;
        out << "      vertical-align : middle;" << endl;
        out << "      max-width : 22px;" << endl;
        out << "      min-width : 22px;" << endl;
        out << "     " << endl;
        out << "}" << endl;
        out << " .timeheader td{" << endl;
        out << "      text-align : center;" << endl;
        out << "      font-weight : bold;" << endl;
        out << "      height : 10px;" << endl;
        out << "      min-height : 10px;" << endl;
        out << "     " << endl;
        out << "}" << endl;
        out << "</style> " << endl;
        out << QString("<title>Расписание:") << title << "</title><body>" << endl;
        out << "<table border='1'>" << endl;
        out << "<tr>" << endl;
        out << "<td class='nameheader' colspan='9'>" << endl;
        out << "<div  align='center'><b>" << title << "</b></div>" << endl;
        out << "</td>" << endl;
        out << "</tr>" << endl;
        out << "<tr class='timeheader'> " << endl;
        out << "<td class = 'daynamecell'> </td> " << endl;
        out << "<td > 08:30 - 10:10 </td> " << endl;
        out << "<td > 10:20 - 12:00 </td> " << endl;
        out << "<td > 12:20 - 14:00 </td> " << endl;
        out << "<td > 14:10 - 15:50 </td> " << endl;
        out << "<td > 16:00 - 17:40 </td> " << endl;
        out << "<td > 18:00 - 19:30 </td> " << endl;
        out << "<td > 19:40 - 21:10 </td> " << endl;
        out << "<td > 21:20 - 22:50 </td> " << endl;
        out << "</tr> " << endl;

        for (int i = 1; i < 7; i++)
        {
            qDebug() << "writing day" << i;

            writeDay(out, i);

        }

        out << "</table>" << endl;
        out << "</body></html>" << endl;



    }


};

#endif	/* HTMLEXPORT_H */

