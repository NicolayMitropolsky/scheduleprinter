#ifndef EXTENDEDQSORTFILTERPROXYMODEL_H
#define EXTENDEDQSORTFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QDebug>

class ExtendedQSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int columnCount READ columnCount)

protected:
    bool lessThan ( const QModelIndex & left, const QModelIndex & right ) const{
        //qDebug()<<"lessThan "<<left<<" "<<right;
        QVariant a = sourceModel()->data(left,Qt::UserRole+left.column());
        QVariant b = sourceModel()->data(right,Qt::UserRole+right.column());
        return a < b;
        //qDebug()<<"data "<<sourceModel()->data(left,Qt::UserRole);
        //return QSortFilterProxyModel::lessThan(left, right);
    }
    bool filterAcceptsRow ( int source_row, const QModelIndex & source_parent ) const{
       //qDebug()<<"filterAcceptsRow "<<source_row<<" "<<source_parent;

       for(int i=0; i < sourceModel()->columnCount(); ++i){
           QVariant a = sourceModel()->data(sourceModel()->index(source_row, i),Qt::UserRole+i);
           //qDebug()<<"a= "<<a;
           //qDebug()<<"filterRegExp()= "<<filterRegExp();
           if(filterRegExp().isEmpty() || filterRegExp().indexIn(a.toString()) >=0)
               return true;
       }
       return false;

       //return QSortFilterProxyModel::filterAcceptsRow(source_row, source_parent);
    }

public:

    explicit ExtendedQSortFilterProxyModel(QObject *parent = 0):QSortFilterProxyModel(parent){
        setDynamicSortFilter(false);
    }

    Q_INVOKABLE QModelIndex mapToSource(const QModelIndex& index) const{
        return  QSortFilterProxyModel::mapToSource(index);
    }

    Q_INVOKABLE int mapToSourceRow(int row) const{
        return  QSortFilterProxyModel::mapToSource(index(row,0)).row();
    }

    Q_INVOKABLE void setSourceModel(QAbstractItemModel* model){
        QSortFilterProxyModel::sort(-1);
        QSortFilterProxyModel::setSourceModel(model);        

    }

    Q_INVOKABLE QVariant headerName(int column) const{
        return  headerData(column,Qt::Horizontal);
    }

    int columnCount() const{
        return  QSortFilterProxyModel::columnCount();
    }

    Q_INVOKABLE void sort(int column, Qt::SortOrder order = Qt::AscendingOrder)
    {
        qDebug()<<"Sorting by column "<<column<<" "<<order;
        QSortFilterProxyModel::sort(column, order);
        beginResetModel();
        endResetModel();
    }



    virtual ~ExtendedQSortFilterProxyModel(){}

};


#endif // EXTENDEDQSORTFILTERPROXYMODEL_H
