#ifndef EXPORTERMANAGER_H
#define EXPORTERMANAGER_H

#include <QObject>
#include "Semestrator.h"
#include <QFile>
#include <QDir>
#include <QDesktopServices>
#include "MultipleRoomsExporter.h"
#include "RoomsExtendedExporter.h"
#include "functional"
#include <algorithm>
#include "TTCPrinter.h"
#include "ExamExporter.h"

enum OutType
{
    Printer,
    PDF
};

class ExporterManager : public QObject
{
Q_OBJECT
    Q_ENUMS(OutType)
    Q_PROPERTY(OutType outType
            READ
            outType
            WRITE
            setOutType)

    Semestrator &semestrator;

    QString lastSubfolder;

    QString removeBad(const QString &orig)
    {
        QString result = orig;
        return result.remove(".").remove("/").remove("(").remove(")").remove("\"");
    }

    OutType _outType = Printer;

    QPrinter *globalPrinter = nullptr;
    QPainter *globalpainter = nullptr;

    bool oneprinted = false;

    QRect previewGeometry;

    bool printWithinPageInterval()
    {
        return globalPrinter != nullptr && globalPrinter->printRange() == QPrinter::PageRange;
    }

public:
    OutType outType() const
    {
        return _outType;
    }

    void setOutType(OutType outType)
    {
        _outType = outType;
    }

    Q_INVOKABLE void setPDFOutput()
    {
        setOutType(PDF);
    }

    Q_INVOKABLE void setPrintingOutput()
    {
        setOutType(Printer);
    }

    explicit ExporterManager(Semestrator &semestrator, QObject *parent = 0) :
            QObject(parent), semestrator(semestrator)
    {
    }

    void showPrintWindow(std::function<void()> paint)
    {
        QPrinter printer(QPrinter::HighResolution);
        //printer.setResolution(1200);
        //printer.setPageMargins(5,5,5,5,QPrinter::Millimeter);
        printer.setOrientation(QPrinter::Landscape);
        QPrintPreviewDialog preview(&printer);
        preview.setWindowFlags(Qt::Window);
        if (previewGeometry.isValid())
            preview.setGeometry(previewGeometry);
        QObject::connect(&preview, &QPrintPreviewDialog::paintRequested, [this, &paint](QPrinter *p) {
            p->setResolution(1200);
            p->setPageMargins(5, 5, 5, 5, QPrinter::Millimeter);
            oneprinted = false;
            globalPrinter = p;
            QPainter painter;
            if (!painter.begin(globalPrinter)) { // failed to open file
                qDebug("failed to open file, is it writable?");
                exit(1);
            }
            globalpainter = &painter;
            paint();
            globalpainter = nullptr;
            globalPrinter = nullptr;

        });
        while (preview.exec());
        previewGeometry = preview.geometry();

    }

    Q_INVOKABLE void write(QString entity, QList<int> ids)
    {
        QPainterExporter exporter;
        _write(entity, ids, exporter);
    }

    Q_INVOKABLE void writeExam(QString entity, QList<int> ids)
    {
        ExamExporter exporter;
        _write(entity, ids, exporter);
    }

    void _write(QString entity, QList<int> ids, QPainterExporter &exporter)
    {
        switch (outType()) {
            case PDF:
                _writeEntity(entity, ids,exporter );
                break;
            case Printer:
                showPrintWindow([&]() {
                    _writeEntity(entity, ids,exporter);
                });
                break;
            default:
                break;
        }
    }

    Q_INVOKABLE void writeEntity(QString entity, QList<int> ids)
    {
        QPainterExporter exporter;
        _writeEntity(entity, ids, exporter);
    }

    Q_INVOKABLE void writeEntityExam(QString entity, QList<int> ids)
    {
        ExamExporter exporter;
        _writeEntity(entity, ids, exporter);
    }

    void _writeEntity(QString entity, QList<int> ids, QPainterExporter& exporter)
    {
        if (printWithinPageInterval()) {
            QList<int> idsnew;

            for (int i = globalPrinter->fromPage(); i <= globalPrinter->toPage() && i <= ids.size(); i++) {
                idsnew.append(ids[i - 1]);
            }

            ids = idsnew;
        }


        if (entity == "group")
            writeGroup(ids, exporter);
        else if (entity == "teacher")
            writeTeacher(ids, exporter);
        else if (entity == "room")
            writeRoom(ids, exporter);
        else {
            qWarning() << "unknown entity:" << entity;
            throw LogicException("unknown entity:" + entity);
        }
    }


    Q_INVOKABLE void writeRoomsWeekly(QList<int> room_ids)
    {
        TTCPrinter pr(true, true, false);
        MultipleRoomsExporter pdfexp(pr);
        writeMultipleRooms(room_ids, pdfexp);
    }

    Q_INVOKABLE void writeMultipleRoomsExtended(QList<int> room_ids)
    {
        TTCPrinter pr(true, true, false);
        RoomsExtendedExporter pdfexp(pr);
        writeMultipleRooms(room_ids, pdfexp);
    }

    void writeMultipleRooms(QList<int> room_ids, MultipleRoomsExporter &pdfexp)
    {
        switch (outType()) {
            case PDF:
                _writeMultipleRooms(room_ids, pdfexp);
                break;
            case Printer:
                showPrintWindow([&]() {
                    _writeMultipleRooms(room_ids, pdfexp);
                });
                break;
            default:
                break;
        }

    }

    void _writeMultipleRooms(QList<int> room_ids, MultipleRoomsExporter &exporter)
    {
        QDir::current().mkdir("roomsweekly");
        lastSubfolder = "roomsweekly";

        QList<Room> rooms;
        QList<SemesterTimeTableCell> tts;

        for (int room_id: room_ids) {
            rooms.append(semestrator.getData().getRooms()[room_id]);
            tts.append(semestrator.genRoom(room_id));

        }

        QDate maxdate(1970, 1, 1);
        QDate mindate(200000, 1, 1);

        for (const SemesterTimeTableCell &cell: tts) {

            if (cell.dates.first() < mindate)
                mindate = cell.dates.first();
            if (cell.dates.last() > maxdate)
                maxdate = cell.dates.last();

        }

        if (!exporter.isWeekDependent()) {
            mindate = mindate.addDays(1 - mindate.dayOfWeek());
            maxdate = mindate.addDays(7);
        }

        qDebug() << "mindate=" << mindate << " maxdate=" << maxdate;

        exporter.setTitle("roomName");
        exporter.setRooms(rooms);
        exporter.setData(tts);

        int fromPage = 0;
        int toPage = 0;

        if (printWithinPageInterval()) {
            fromPage = globalPrinter->fromPage();
            toPage = globalPrinter->toPage();
        }

        int totalPrintedPages = 0;

        QDate ws = mindate.addDays(1 - mindate.dayOfWeek());
        int week = 1;

        for (; ws < maxdate; ws = ws.addDays(7)) {

            exporter.setInterval(ws, ws.addDays(6));

            if (printWithinPageInterval()) {
                exporter.fromPage = std::max(1, fromPage - totalPrintedPages);
                exporter.toPage = toPage - totalPrintedPages;
                qDebug() << "exporter.fromPage=" << exporter.fromPage << " exporter.toPage=" << exporter.toPage;
                if (exporter.toPage <= 0)
                    break;
            }

            QString fname = "roomsweekly/" + removeBad(rooms.first().name)
                    + "-" + removeBad(rooms.last().name)
                    + "-" + QString::number(week) + ".pdf";

            qDebug() << "fname=" << fname;

            if (fromPage > totalPrintedPages) {
                oneprinted = false;
            }

            runExport(exporter, fname);
            totalPrintedPages += exporter.toPage;


            week++;

        }

    }


    Q_INVOKABLE void openLastExportFolder()
    {
        QString path = QDir::toNativeSeparators(QDir::current().filePath(lastSubfolder));
        QDesktopServices::openUrl(QUrl("file:///" + path));
    }

private:

    void runPdfExport(QPainterExporter &exporter, QString filename)
    {
        QPrinter printer(QPrinter::HighResolution);
        //printer.setResolution(1200);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOrientation(exporter.orientationHint());
        qDebug() << "writing filename:" << filename;
        printer.setOutputFileName(filename);

        exporter.runExport(printer);
    }

    void runExport(QPainterExporter &exporter, const QString &fname)
    {

        switch (outType()) {
            case PDF:
                runPdfExport(exporter, fname);
                break;
            case Printer:
                if (oneprinted)
                    globalPrinter->newPage();
                globalPrinter->height();
                globalPrinter->setOrientation(exporter.orientationHint());
                //globalpainter->scale(2., 2.);
                exporter.runExport(*globalPrinter, *globalpainter);

                oneprinted = true;
                break;
            default:
                break;
        }


    }

    void writeGroup(QList<int> groupIds, QPainterExporter& exporter)
    {
        QDir::current().mkdir("groups");
        lastSubfolder = "groups";

        for (auto groupId: groupIds) {

            QList<SemesterTimeTableCell> grouptts = semestrator.genGroup(groupId);

            Group gr = semestrator.getData().getGroups()[groupId];
            lastSubfolder = "groups/" + QString::number(gr.semestr);
            QDir::current().mkdir(lastSubfolder);
            QString groupName = gr.name;
            TTCPrinter pr(false, true, true);
            exporter.setTtcPrinter(pr);

            exporter.setTitle(groupName);

            exporter.setData(grouptts);

            runExport(exporter, lastSubfolder + "/" + groupName + ".pdf");
        }
    }

    void writeTeacher(QList<int> teacherIds, QPainterExporter& exporter)
    {
        QDir::current().mkdir("teachers");
        lastSubfolder = "teachers";

        for (auto teacherId: teacherIds) {

            QList<SemesterTimeTableCell> tts = semestrator.genTeacher(teacherId);

            Teacher t = semestrator.getData().getTeachers()[teacherId];

            Chair c = semestrator.getData().getChairs()[t.chair_id];

            QString teacherName = t.tname();

            TTCPrinter pr(true, false, true);
            exporter.setTtcPrinter(pr);

            exporter.setTitle(teacherName);

            exporter.setData(tts);

            lastSubfolder = "teachers/" + removeBad(c.short_name);

            QDir::current().mkdir(lastSubfolder);

            runExport(exporter, lastSubfolder + "/" + teacherName.remove(".") + "-" + QString::number(teacherId) + ".pdf");

        }

    }

    void writeRoom(QList<int> room_ids, QPainterExporter& exporter)
    {
        QDir::current().mkdir("rooms");
        lastSubfolder = "rooms";

        for (auto room_id: room_ids) {

            QList<SemesterTimeTableCell> tts = semestrator.genRoom(room_id);

            QString roomName = semestrator.getData().getRooms()[room_id].name;

            TTCPrinter pr(true, true, false);
            exporter.setTtcPrinter(pr);

            exporter.setTitle(roomName);

            exporter.setData(tts);

            QString fname = "rooms/" + removeBad(roomName) + "-" + QString::number(room_id) + ".pdf";

            qDebug() << "fname=" << fname;

            runExport(exporter, fname);

        }

    }


signals:

public slots:

};

#endif // EXPORTERMANAGER_H
