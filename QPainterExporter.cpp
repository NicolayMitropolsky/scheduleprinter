/* 
 * File:   PDFExporter.cpp
 * Author: nickl
 *
 * Created on March 22, 2014, 5:44 PM
 */

#include "QPainterExporter.h"


void QPainterExporter::drawRotatedText(QPainter& painter, int x, int y, int width, int height, const QString& text)
{
    painter.save();
    painter.translate(x, y);
    painter.rotate(270);
    painter.drawText(0, 0, width, height, Qt::AlignVCenter | Qt::AlignCenter, text);
    painter.restore();
}
void QPainterExporter::adaptFontSize(QPainter& painter, int flags, QRectF rect, QString text, QFont font)
{
    QRect fontBoundRect;
    fontBoundRect = painter.fontMetrics().boundingRect(rect.toRect(), flags, text);
    while (rect.width() < fontBoundRect.width() || rect.height() < fontBoundRect.height())
    {
        font.setPointSizeF(font.pointSizeF()*0.95);
        //font.setLetterSpacing(font.letterSpacingType(), font.letterSpacing() * 0.7);
        painter.setFont(font);
        fontBoundRect = painter.fontMetrics().boundingRect(rect.toRect(), flags, text);
    }
}
int QPainterExporter::maxLen(const QList< SemesterTimeTableCell >& ttsl)
{

    int res = 1;

    for (const SemesterTimeTableCell& ttc : ttsl)
    {
        int ns = ttc.hours.size();
        if (ns > res)
            res = ns;
    }

    return res;
}
void QPainterExporter::writeDay(QPainter& painter, int day)
{
    //out << "<tr>" << endl;

    auto dayttcs = timetableByDay[day];
    //qDebug("");
    //qDebug()<< " day="<< day;

    //        for(auto k: dayttcs.keys()){
    //            qDebug()<< k <<" -> "<< dayttcs[k];
    //        }

    auto complexHours = retriveComplexHours(dayttcs);
    //qDebug()<<"complexHours="<<complexHours;

    int prevBase = 0;
    for (int i = 0; i < hoursIndxs.size();)
    {

        painter.drawLine(verts[i + 1], hors[day], verts[i + 1], hors[day + 1]);

        int hi = hoursIndxs[i];


        QList<SemesterTimeTableCell>& ds = dayttcs[hi];

        //qDebug()<< "i="<< i << " di=" << di << " ds =" << ds;

        int hl = maxLen(ds);

        if (!complexHours.contains(hi))
        {
            qDebug()<< "not complex";
            drawInOne(painter, day, i, ds, hl);
        }
        else {
            qDebug() << "complex";
            QList<SemesterTimeTableCell> fcttc = filter(ds, [](const SemesterTimeTableCell &tts) {
                return tts.hours.size() >= 2;
            });

            QList<SemesterTimeTableCell> scttc = filter(ds, [](const SemesterTimeTableCell &tts) {
                return tts.hours.size() == 1;
            });
            scttc.append(filter(dayttcs[hi - 1], [hi](const SemesterTimeTableCell &tts) {
                return tts.hours.contains(hi);
            }));

            if(fcttc.isEmpty())
                drawInOne(painter, day, i, scttc, hl);
            else {
                QList<SemesterTimeTableCell> tcttc = dayttcs[hi + 1];

                QString firstCellText = printTTCs(fcttc);
                QString secondCellText = printTTCs(scttc);
                QString thirdCellText = printTTCs(tcttc);

                int midH = getHorDivider(day,
                        firstCellText.size() / 2,
                        std::max(secondCellText.size(), thirdCellText.size())
                );

                painter.drawLine(verts[i + 1], midH, verts[i + 1 + hl], midH);
                painter.drawLine(verts[i + 2], midH, verts[i + 2], hors[day + 1]);

                QRectF br1(verts[i + 1] + padding, hors[day] + padding,
                        verts[i + 1 + hl] - verts[i + 1] - padding * 2, midH - hors[day] - padding * 2);

                drawFitToRect(painter, br1, firstCellText);

                QRectF br2(verts[i + 1] + padding, midH + padding,
                        verts[i + 2] - verts[i + 1] - padding * 2, hors[day + 1] - midH - padding * 2);

                drawFitToRect(painter, br2, secondCellText);

                QRectF br3(verts[i + 2] + padding, midH + padding,
                        verts[i + 3] - verts[i + 2] - padding * 2, hors[day + 1] - midH - padding * 2);

                drawFitToRect(painter, br3, thirdCellText);

            }

        }

        prevBase = i;
        i += hl;

    }


}

void QPainterExporter::drawInOne(QPainter &painter, int day, int i, QList<SemesterTimeTableCell> &ds, int hl)
{
    QRectF br(verts[i + 1] + padding, hors[day] + padding,
                    verts[i + 1 + hl] - verts[i + 1] - padding * 2, hors[day + 1] - hors[day] - padding * 2);

    drawFitToRect(painter, br, printTTCs(ds));
}

int QPainterExporter::getHorDivider(int day, int ceilSize, int floorsize)
{
    int ffs = std::sqrt(std::max(ceilSize, 10));
    int sfs = std::sqrt(std::max(floorsize, 10));
    float proportio = 1.0 * ffs / (ffs + sfs);
    int midH = hors[day] + (hors[day + 1] - hors[day]) * proportio;
    return midH;
}

void QPainterExporter::drawFitToRect(QPainter& painter, QRectF br, QString text)
{
    QFont oldfont = painter.font();

    QFont f = getFont(smallFont());

    painter.setFont(f);

    int flags = Qt::AlignTop | Qt::AlignLeft | Qt::TextWordWrap;

    adaptFontSize(painter, flags, br, text, f);

    painter.drawText(br, flags, text);

    painter.setFont(oldfont);
}
void QPainterExporter::runExport(QPrinter& printer) {
    QPainter painter;
    if (!painter.begin(&printer))
    { // failed to open file
        qDebug("failed to open file, is it writable?");
        exit(1);
    }

    runExport(printer, painter);

    painter.end();

}
void QPainterExporter::runExport(QPrinter& printer, QPainter& painter)
{

    qDebug() << "hw=" << printer.height() << " " << printer.width();

    qDebug() << "pagesize=" << printer.pageRect();

    //float v = 13;

    v = printer.pageRect().width() / 1000;
    padding = v * 1.5;

    //qDebug() << "data=" << timetableByDay;

    verts = QList<int>();
    hors = QList<int>();
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);

    QPen pen;
    pen.setWidth(15);

    painter.setPen(pen);

    verts.append(30 * v);
    verts.append(verts.last() + 15*v);
    for (int i = 0; i < 8; i++)
    {
        verts.append(verts.last() + 120*v);
    }

    hors.append(70 * v);
    hors.append(hors.last() + 15*v);
    for (int i = 0; i < 6; i++)
    {
        hors.append(hors.last() + 100*v);
    }


    for (int j = 0; j < hors.size(); j++)
    {
        painter.drawLine(verts.first(), hors[j], verts.last(), hors[j]);
    }


    painter.drawLine(verts[0], hors[0], verts[0], hors[hors.size() - 1]);
    painter.drawLine(verts[1], hors[0], verts[1], hors[hors.size() - 1]);
    painter.drawLine(verts[verts.size() - 1], hors[0], verts[verts.size() - 1], hors[hors.size() - 1]);


    for (int j = 1; j < verts.size() - 1; j++)
    {
        painter.drawLine(verts[j], hors[0], verts[j], hors[1]);
    }



    //qDebug() << "clipBoundingRect=" << painter.clipBoundingRect();
    //qDebug() << "viewport=" << painter.viewport();
    //qDebug() << "window=" << painter.window();

    QFont ft = getFont(bigFont());

    painter.setFont(ft);

    painter.drawText(verts[0], 20 * v,
            verts[verts.size() - 1] - verts[0], 40 *v,
            Qt::AlignVCenter | Qt::AlignCenter,
            title);



    QFont f = getFont(smallFont());
    painter.setFont(f);

    for (int i = 0; i < hours.size(); i++)
    {
        int h = i + 1;
        painter.drawText(verts[h], hors[0],
                verts[h + 1] - verts[h], hors[1] - hors[0],
                Qt::AlignVCenter | Qt::AlignCenter,
                hours[i]);

    }


    for (int i = 0; i < daynamesFull.size(); i++)
    {
        int h = i + 2;
        drawRotatedText(painter, verts[0], hors[h], hors[h] - hors[h - 1], verts[1] - verts[0], daynamesFull[i]);
    }

    for (int i = 1; i < 7; i++)
    {
        //qDebug() << "writing day" << i;

        writeDay(painter, i);

    }

}
