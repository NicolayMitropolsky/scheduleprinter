#ifndef EXAMEXPORTER_H
#define EXAMEXPORTER_H

#include "QPainterExporter.h"

class ExamSubject
{
public:
    Subject subject;
    QList<SemesterTimeTableCell> exam;
    QList<SemesterTimeTableCell> consultation;


    ExamSubject(Subject const &subject, QList<SemesterTimeTableCell> const &exam, QList<SemesterTimeTableCell> const &consultation)
            : subject(subject), exam(exam), consultation(consultation)
    {

        auto cmr = [](const SemesterTimeTableCell &ttc1, const SemesterTimeTableCell &ttc2) {
            return std::make_tuple(ttc1.dates.first(), ttc1.hours.first()) <
                    std::make_tuple(ttc2.dates.first(), ttc2.hours.first());
        };

        qStableSort(this->exam.begin(), this->exam.end(), cmr);
        qStableSort(this->consultation.begin(), this->consultation.end(), cmr);
    }


    std::tuple<QDate, int> examStart() const
    {
        return std::make_tuple(examDate(), examStartHour());
    }

    int examStartHour() const
    {
        return exam.first().hours.first();
    }

    QDate examDate() const
    {
        return exam.first().dates.first();
    }

    std::tuple<QDate, int> examEnd() const
    {
        return std::make_tuple(exam.last().dates.last(), examEndHour());
    }

    int examEndHour() const
    {
        return exam.last().hours.last();
    }

    std::tuple<QDate, int> consStart() const
    {
        return std::make_tuple(consultDate(), consultStartHour());
    }

    int consultStartHour() const
    {
        return consultation.first().hours.first();
    }

    QDate consultDate() const
    {
        return consultation.first().dates.first();
    }

    std::tuple<QDate, int> consEnd() const
    {
        return std::make_tuple(consultation.last().dates.last(), consultEndHour());
    }

    int consultEndHour() const
    {
        return consultation.last().hours.last();
    }


};

QDebug &operator<<(QDebug &stream, const ExamSubject &t);


class ExamExporter : public QPainterExporter
{

    QList<ExamSubject> examSubjects;

    QHash<int, QString> hstarts = {
            {1, "8:30"},
            {2, "10:20"},
            {3, "12:20"},
            {4, "14:10"},
            {5, "16:00"},
            {6, "18:00"},
            {7, "19:40"},
            {8, "21:20"}
    };

    QHash<int, QString> hends = {
            {1, "10:10"},
            {2, "12:00"},
            {3, "14:00"},
            {4, "15:50"},
            {5, "17:40"},
            {6, "19:30"},
            {7, "21:10"},
            {8, "22:50"}
    };


public:
    ExamExporter(TTCPrinter const &ttcPrinter) : QPainterExporter(ttcPrinter)
    {
    }

    ExamExporter() : QPainterExporter()
    {
    }

    virtual QPrinter::Orientation orientationHint()
    {
        return QPrinter::Portrait;
    }

    virtual void setData(const QList<SemesterTimeTableCell> &data) override
    {

        qDebug() << "ExamExporter arg=" << data;

        timetableByDay.clear();
        examSubjects.clear();

        QMap<std::tuple<int, int, int>, QList<SemesterTimeTableCell>> subjmap;

        for (const SemesterTimeTableCell &cell : data) {
            subjmap[std::make_tuple(cell.subject.id, cell.teacher.id, cell.groups.first().id)].append(cell);
        }

        for (const QList<SemesterTimeTableCell> subjCells : subjmap.values()) {

            QList<SemesterTimeTableCell> exams;
            QList<SemesterTimeTableCell> consults;

            for (const SemesterTimeTableCell &ttc: subjCells) {
                qDebug() << "type" << ttc.studyType.full_name;
                if (ttc.studyType.full_name == QString("экзамен")) {
                    exams.append(ttc);
                }
                else if (ttc.studyType.full_name == QString("консультация"))
                    consults.append(ttc);
            }

            qDebug() << "exams" << exams;
            qDebug() << "consults" << consults;

            if (!exams.isEmpty())
                examSubjects.append(ExamSubject(subjCells.first().subject, exams, consults));

        }

        qStableSort(examSubjects.begin(), examSubjects.end(), [](const ExamSubject &e1, const ExamSubject &e2) {
            return e1.examStart() < e2.examStart();
        });

    }


    float titleX = 30 * v;
    float titleY = 30 * v;

    float leftX = titleX;
    float upperY = titleY + 50 * v;

    float dateLen = 120 * v;
    float subjlen = 520 * v;

    float cellHeight = 100 * v;
    float consHeight = 30 * v;
    float examHeight = cellHeight - consHeight;

    int smallFont() override
    {
        return v * 0.85;
    }

    void runExport(QPrinter &printer, QPainter &painter) override
    {

        QPen pen;
        pen.setWidth(45);
        painter.setPen(pen);

        QPen thinPen;
        thinPen.setWidth(10);

        QFont ft = getFont(bigFont());
        painter.setFont(ft);

        painter.drawText(titleX, titleY,
                dateLen + subjlen, 40 * v,
                Qt::AlignVCenter | Qt::AlignCenter,
                title);

        QFont f = getFont(smallFont());
        painter.setFont(f);

        painter.drawLine(QLineF(leftX, upperY, leftX + dateLen + subjlen, upperY));

        painter.drawText(QRectF(leftX, upperY - 30 * v,
                        dateLen, 30 * v),
                Qt::AlignVCenter | Qt::AlignCenter, "экзамены");

        int i = 0;
        for (const ExamSubject &examSubject: examSubjects) {
            float boxUpperY = upperY + cellHeight * i;

            painter.drawLine(QLineF(leftX, boxUpperY, leftX + dateLen + subjlen, boxUpperY));

            qDebug() << examSubject.subject.full_name;

            if (!examSubject.consultation.isEmpty())
                painter.drawText(QPointF(leftX + 300 * v, boxUpperY + consHeight - 10 * v),
                        QString("консультация: ") + examSubject.consultDate().toString("dd.MM.yyyy")
                                + " " + hstarts[examSubject.consultStartHour()]
                                + " ауд. " + examSubject.consultation.first().place.name);

            painter.save();
            painter.setPen(thinPen);
            painter.drawLine(QLineF(leftX, boxUpperY + consHeight, leftX + dateLen + subjlen, boxUpperY + consHeight));
            painter.restore();

            painter.drawText(QRectF(leftX, boxUpperY + consHeight + 5 * v,
                            dateLen, examHeight / 2),
                    Qt::AlignVCenter | Qt::AlignCenter,
                    examSubject.examDate().toString("dd.MM.yyyy"));

            painter.drawText(QRectF(leftX, boxUpperY + consHeight + examHeight / 2 + 5 * v,
                            dateLen, examHeight / 2 - 15 * v),
                    Qt::AlignVCenter | Qt::AlignCenter,
                    daynamesFull[examSubject.exam.first().day - 1]);

            painter.drawText(QPointF(leftX + dateLen + 20 * v, boxUpperY + consHeight + 25 * v),
                    hstarts[examSubject.examStartHour()] + " - " + hends[examSubject.examEndHour()] +
                            " ауд. " + examSubject.exam.first().place.name);

            painter.drawText(QPointF(leftX + dateLen + 220 * v, boxUpperY + consHeight + 25 * v),
                    parcipients(examSubject)
            );
            painter.drawText(QPointF(leftX + dateLen + 20 * v, boxUpperY + consHeight + examHeight / 2 + 20 * v),
                    examSubject.subject.full_name
            );

            ++i;
        }

        float lowerY = upperY + cellHeight * i;
        painter.drawLine(QLineF(leftX, upperY, leftX, lowerY));
        painter.drawLine(QLineF(leftX + dateLen, upperY, leftX + dateLen, lowerY));
        painter.drawLine(QLineF(leftX + dateLen + subjlen, upperY, leftX + dateLen + subjlen, lowerY));

        painter.drawLine(QLineF(leftX, lowerY, leftX + dateLen + subjlen, lowerY));


    }

    QString parcipients(ExamSubject const &examSubject)
    {
        if (ttcPrinter.printGroup)
            return examSubject.exam.first().groups.first().name;
        else
            return examSubject.exam.first().teacher.tname();
    }

};

#endif
