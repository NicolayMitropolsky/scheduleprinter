#ifndef ROOMTABLEMODEL_H
#define ROOMTABLEMODEL_H

#include <QObject>
#include "EntityTableModel.h"

class RoomTableModel  : public EntityTableModel
{
    Q_OBJECT

    QList<QVariant> headers = {
        QVariant::fromValue(QString("Название")),
        QVariant::fromValue(QString("Размер"))
    };

public:
    explicit RoomTableModel(Semestrator& semestrator, QObject *parent = 0):EntityTableModel(semestrator, parent){}

    QVariant data(const QModelIndex & index, int role) const{

        //qDebug()<<"data called " + QString::number(role);

        if(role < Qt::UserRole)
        {
            return QVariant();
        }

        role = role - Qt::UserRole;

        auto groups = semestrator.getData().getRooms();
        if(groups.size() == 0)
            return QVariant();


        if(role == 0)
            return  QVariant::fromValue(groups[groups.keys()[index.row()]].name);
        else
            return  QVariant::fromValue(groups[groups.keys()[index.row()]].capacity);
    }



    int rowCount(const QModelIndex & parent) const{

        return semestrator.getData().getRooms().size();

    }

    int columnCount(const QModelIndex & parent) const{
        //qDebug()<<"columnCount called";
        return headers.size();
    }

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const{
        qDebug()<<"headerData called "<<role;
        if(role != Qt::DisplayRole)
            return QVariant();

        return  headers[section];
    }

    int getId(int colnum){
        return semestrator.getData().getRooms().keys()[colnum];
    }

    QString getEname() {
        return "room";
    }


};

#endif // ROOMTABLEMODEL_H
