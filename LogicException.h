#ifndef LOGICEXCEPTION_H
#define LOGICEXCEPTION_H

#include <QString>

class LogicException
{
public:
    QString message;
    LogicException();
    LogicException(QString message):message(message){}
};

#endif // LOGICEXCEPTION_H
