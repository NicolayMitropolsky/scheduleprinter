# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Release/GNU-Linux-x86
TARGET = SchedulePrinter
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += release 
PKGCONFIG +=
QT = core gui widgets
SOURCES += /home/nickl/NetBeansProjects/SchedulePrinter/HtmlExport.cpp /home/nickl/NetBeansProjects/SchedulePrinter/Items.cpp /home/nickl/NetBeansProjects/SchedulePrinter/QPainterExporter.cpp /home/nickl/NetBeansProjects/SchedulePrinter/Semestrator.cpp /home/nickl/NetBeansProjects/SchedulePrinter/TTCPrinter.cpp /home/nickl/NetBeansProjects/SchedulePrinter/XMLDataReader.cpp EntityTableModel.cpp ExporterManager.cpp ExtendedQSortFilterProxyModel.cpp GroupTableModel.cpp LogicException.cpp MultipleRoomsExporter.cpp RoomTableModel.cpp RoomsExtendedExporter.cpp TeacherTableModel.cpp main.cpp main0.cpp qtquick2controlsapplicationviewer/qtquick2controlsapplicationviewer.cpp settings.cpp
HEADERS += /home/nickl/NetBeansProjects/SchedulePrinter/HtmlExport.h /home/nickl/NetBeansProjects/SchedulePrinter/Items.h /home/nickl/NetBeansProjects/SchedulePrinter/QPainterExporter.h /home/nickl/NetBeansProjects/SchedulePrinter/Semestrator.h /home/nickl/NetBeansProjects/SchedulePrinter/TTCPrinter.h /home/nickl/NetBeansProjects/SchedulePrinter/XMLDataReader.h /home/nickl/NetBeansProjects/SchedulePrinter/utils.h EntityTableModel.h ExporterManager.h ExtendedQSortFilterProxyModel.h GroupTableModel.h LogicException.h MultipleRoomsExporter.h RoomTableModel.h RoomsExtendedExporter.h Settings.h TeacherTableModel.h qtquick2controlsapplicationviewer/qtquick2controlsapplicationviewer.h
FORMS +=
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Release/GNU-Linux-x86
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc
QMAKE_CXX = g++
DEFINES += 
INCLUDEPATH += 
LIBS += 
