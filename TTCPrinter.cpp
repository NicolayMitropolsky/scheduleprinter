/* 
 * File:   TTCPrinter.cpp
 * Author: nickl
 *
 * Created on May 10, 2014, 9:20 AM
 */

#include "TTCPrinter.h"

#include "utils.h"


QString TTCPrinter::printTTC(const SemesterTimeTableCell& tt)
{
    QString grName = "";

    if (printGroup)
    {
        QList<QString> grps = convert<Group, QString>(tt.groups, [](const Group & gr) {
            return gr.name;
        });

        QStringList grpssl(grps);
        grpssl.sort();
        grName = grpssl.join(", ") + ". ";
    }

    QString subgroup = "";
    if (tt.loadPart.loadPartsCount == 2)
    {
        int lpi = tt.loadPart.loadPartIndex;
        subgroup = lpi == 0 ? "(А). " : lpi == 1 ? "(Б). " : "(?). ";
    }

    QString teacherStr = "";
    if (!tt.teacher.surname.contains("=") && printTeacher)
        teacherStr = tt.teacher.tname() + " ";

    return grName + tt.subject.full_name + ".\n"
            + teacherStr
            + tt.studyType.full_name + ". " + subgroup
            + (printKlass ? tt.place.name + ". " : "") + "[" + datesString(tt.dates) + "]";
}
QString TTCPrinter::printTTCs(QList< SemesterTimeTableCell > ttcs)
{
    QStringList res;

    qSort(ttcs.begin(), ttcs.end(), [](SemesterTimeTableCell& a, SemesterTimeTableCell & b) {
        return a.dates.first() < b.dates.first();
    });

    for (auto const & ttc : ttcs)
    {
        res.append(printTTC(ttc));
    }

    return res.join("\n");
}
QString TTCPrinter::datesString(const QList< QDate >& dates)
{

    //qDebug()<<"dates="<<dates;

    if (dates.size() < 2)
    {
        auto ds = convert<QDate, QString>(dates, [](const QDate & date) {
            return date.toString("dd.MM");
        });

        return QStringList(ds).join(", ");
    }



    QList<int> intervals;

    for (int i = 0; i < dates.size() - 1; i++)
    {
        intervals.append(dates[i + 1].dayOfYear() - dates[i].dayOfYear());
    }

    //qDebug() << "intervals=" << intervals;

    QList < std::pair<int, int >> repetions = detectRepeation(intervals, 1);

    int j = 0;

    QStringList res;



    for (std::pair<int, int> startend : repetions)
    {

        //qDebug() << "repetion=" << startend.first << " " << startend.second;


        for (int i = j; i < startend.first; i++)
        {
            res.append(dates[i].toString("dd.MM"));
        }

        int s = std::max(j, startend.first);

        if (startend.second - s > 0)
        {
            QString intervalString;
            intervalString += dates[s].toString("dd.MM");
            intervalString += "-";
            intervalString += dates[startend.second + 1].toString("dd.MM");
            //qDebug() << "startend.first=" << startend.first;
            //qDebug() << "intervals[startend.first]=" << intervals[startend.first];
            switch (intervals[startend.first])
            {
            case 7: intervalString += " к.н.";
                break;
            case 14: intervalString += " ч.н.";
                break;
            default: intervalString += (QString(" к.") + intervals[startend.first] / 7 + " н");

            }
            res += intervalString;
            j = startend.second + 2;
        }
        else
            j = s;


    }

    for (int i = j; i < dates.size(); i++)
    {
        res.append(dates[i].toString("dd.MM"));
    }

    //qDebug()<<"res="<<res;

    return res.join(", ");


}
QList< std::pair< int, int > > TTCPrinter::detectRepeation(QList< int > data, int l)
{

    QList < std::pair<int, int >> result;

    int si = 0;
    int rep = data[0];
    for (int i = 0; i < data.size(); i++)
    {
        if (data[i] != rep)
        {
            if (i - si > l)
                result += std::pair<int, int > (si, i - 1);
            rep = data[i];
            si = i;
        }
    }

    if (rep == data[data.size() - 1] && data.size() - si > l)
        result += std::pair<int, int > (si, data.size() - 1);

    return result;
}
